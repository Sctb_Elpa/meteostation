/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2013        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */

#include "Lib/DataflashManager.h"
#include "Lib/Fatfs/ffconf.h"
#include "ds1307.h"
#include "debug.h"
#include "Lib/24cXXX.h"
#include "SoftI2CMaster.h"


/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber (0..) */
)
{
	if (pdrv) 
		return STA_NOINIT;
	return 0;
}



/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber (0..) */
)
{
	if (pdrv)
		return STA_NOINIT;
	return 0;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address (LBA) */
	UINT count		/* Number of sectors to read (1..128) */
)
{
	//DEBUG_MSG("R: %li, c=%i\t", (long)sector, (int)count);
	if (pdrv || !count) 
		return RES_PARERR;
	
	DataflashManager_ReadBlocks_RAM(sector, count, buff);
	
	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

extern uint32_t sect;

#if _USE_WRITE
DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber (0..) */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address (LBA) */
	UINT count			/* Number of sectors to write (1..128) */
)
{
	//DEBUG_MSG("R: %li, c=%i\t", (long)sector, (int)count);
	if (!count || pdrv) 
		return RES_PARERR;
			
	DataflashManager_WriteBlocks_RAM(sector, count, buff);
	
	return RES_OK;
}
#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res = RES_ERROR;
	
	if (pdrv)
		return RES_PARERR;
	
	switch (cmd)
	{
	case CTRL_SYNC:
		while (!EEBegin(0))
			i2c_stop();
		i2c_stop();
		res = RES_OK;
		break;
	case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (DWORD) */
		*(DWORD*)buff = VIRTUAL_MEMORY_BYTES / _MAX_SS;
		res = RES_OK;
		break;
	case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */
		*(DWORD*)buff = 1;
		res = RES_OK;
		break;
	default:
		res = RES_PARERR;
	}
	return res;
}
#endif

DWORD get_fattime (void)
{
	return ((DWORD)(20 + dataOutputVars.dateTime.year) << 25) |
		((DWORD)dataOutputVars.dateTime.month << 21) |
		((DWORD)dataOutputVars.dateTime.day << 16) |
		((DWORD)dataOutputVars.dateTime.hour << 11) |
		((DWORD)dataOutputVars.dateTime.minute << 5) |
		(((DWORD)dataOutputVars.dateTime.second >> 1) << 0);
}