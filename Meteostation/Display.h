/*
 * Display.h
 *
 * Created: 15.05.2014 13:03:09
 *  Author: tolyan
 */ 


#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdbool.h>

#include "ds1307.h"
#include "Shaduler.h"

#if DOUBLE_REFRASH_RATE == 1
 #define POPUP_AUTOCLOSE			10
#else
 #define POPUP_AUTOCLOSE			5
#endif

enum enHeaderStatus
{
	H_BATAREY_0 = 0, 
	H_BATAREY_1 = 1,
	H_BATAREY_2 = 2,
	H_BATAREY_3 = 3,
	H_BATAREY_4 = 4,
	H_BAT_MASK_VAL = 0b111,
	H_BATAREY_CHARGE = 1 << 3,
	H_BAT_MASK = 0b1111,
	
	H_BLUETOOTH = 1 << 5,
	H_RECORDING = 1 << 6,
	
	MEMORY_UNMOUNTED = 0x1000,
	MEMORY_FILL_MASK_VAL = 0x0f00,
	MEMORY_FILL_MASK = MEMORY_FILL_MASK_VAL | MEMORY_UNMOUNTED,
};

enum enPopUP
{
	POPUP_NONE = 0,
	POPUP_USB_CONNECTED = 1,
	POPUP_USB_DISCONNECTED = 2,
	POPUP_BT_ENABLED = 3,
	POPUP_BT_CONNECTED = 4,
	POPUP_BT_DISABLED = 5,
	POPUP_BT_DISCONNECTED = 6,
	POPUP_SAVING_MESURE = 7,
	POPUP_SELECT_SAVE_INTERVAL = 8,
	POPUP_SAVING_STARTED = 9,
	POPUP_SAVING_STOPPED = 10,
	POPUP_ASK_CLEAR_MEMORY = 11,
	POPUP_MEMORY_CLEARED = 12,
	POPUP_MEMORY_FULL = 13,
};

#define USB_REPORT(x)

enum usbReportType
{
	USB_Device_Connect = 0,
	USB_Device_Disconnect = 1,
	USB_Device_ConfigurationChanged = 2,
	USB_Device_ControlRequest = 3,
	USB_Device_StartOfFrame = 4,
	HID_Device_CreateHIDReport = 5,
	HID_Device_ProcessHIDReport = 6
};

typedef struct  
{
	 struct ds1307DateTime dateTime;
	 float Temperature;
	 float Pressure;
	 float Humidity;
	 enum enHeaderStatus headerStatus;
	 enum enPopUP popUp;
	 uint16_t	ADC_RAWVal;
	 char* dbgStr;
} sDisplay_vars;

#define TEMPERATURE_FORMAT			"% 6.2f" //"%+6.2f" //C+
#define PRESSURE_PORMAT				"%6.2f"
#define HUMIDITY_FORMAT				"%6.2f"

extern const char TemperatureFormat[];
extern const char PressureFormat[];
extern const char HumidityFormat[];

// hard == 1 - ������������ ���������
void RefrashDisplay();

void prepareScreen();
void drawHeader();
void drawTemperature(char* buf);
void drawPreassure(char* buf);
void drawHumidity(char *buf);
void drawDate(char* buf);

void display_on();
void display_off();

uint8_t checkDisplayEnabled();

// �������� ��������� �������� ����� �� ������
uint8_t updateUsedSpaceIndicator(float precent);

extern sDisplay_vars dataOutputVars;
extern uint8_t forceDisplayRefrash;

#endif /* DISPLAY_H_ */