/*
 * Display.c
 *
 * Created: 15.05.2014 13:00:33
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <avr/eeprom.h>

#include <src/config/AppConfig.h>
#include <LUFA/Drivers/Board/Dataflash.h>

#include "src/config/AppConfig.h"

#include "SSD1306.h"
#include "UsbConnectISR.h"

#include "Fonts.h"
#include "Lib/24cXXX.h"
#include "SoftI2CMaster.h"
#include "SSD1306.h"
#include "Settings.h"
#include "Shaduler.h"
#include "UI.h"
#include "debug.h"

#include "Display.h"

#define TEMPERATURE_POSITION_X						0
#define TEMPERATURE_POSITION_Y						2

#define PRESSURE_CENTER_POSITION_X					(80)
#define PRESSURE_POSITION_Y							2

#define HUMIDITY_POSITION_X							0
#define HUMIDITY_POSITION_Y							6

#define DATE_TIME_POSITION_X						(47 + 9)
#define DATE_DATE_POSITION_X						(DATE_TIME_POSITION_X + 4 * 9 + 3 + 13 - 9)
#define DATE_POSITION_Y								6

#define SYMBOL_RIGHT_DOWN_DOUBLE_LINES_CONER		187 //ok
#define SYMBOL_RIGHT_UP_DOUBLE_LINES_CONER			186 //ok
#define SYMBOL_LEFT_DOWN_DOUBLE_LINES_CONER			199
#define SYMBOL_LEFT_UP_DOUBLE_LINES_CONER			200
#define SYMBOL_HORISONTAL_DOUBLE_LINES_CONER		204
#define SYMBOL_VERTICAL_DOUBLE_LINES_CONER			185

struct sPopupText
{
	const char* text;
	const uint8_t Snowflake_enabled;	
};

static char message[64] = "";

uint8_t forceDisplayRefrash;


static const char M1[] PROGMEM = "USB\nConnected";
static const char M2[] PROGMEM = "USB\nDisconnected";
static const char M3[] PROGMEM = "Bluetooth\nENABLED";
static const char M4[] PROGMEM = "Bluetooth\nconnected";
static const char M5[] PROGMEM = "Bluetooth\nDISABLED";
static const char M6[] PROGMEM = "Bluetooth\ndisconnected";
static const char M7[] PROGMEM = "Saving current\nmesure..";
static const char M8[] PROGMEM = "Select interval:\n                                                    ";
static const char M9[] PROGMEM = "Now saving\nenabled";
static const char M10[] PROGMEM = "Now saving\nconceled";
static const char M11[] PROGMEM = 
#if DISPLAY_ORIENTATION == 1
"\t\t[  NO ]\nClean memory?\n\t\t[ YES ]";
#else
"\t\t[ YES ]\nClean memory?\n\t\t[  NO ]";
#endif
static const char M12[] PROGMEM = "Cleaning\nmemory..";
static const char M13[] PROGMEM = "Memory is\nfull!";

static const uint8_t disply_background[] 
#if DISPLAY_BACKGROUNG_STORE_EEPROM == 1
EEMEM
#else
PROGMEM 
#endif
= {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x7C, 0x04, 0x00, 0x7C, 0x54, 0x44, 0x00, 0x7C, 0x08, 0x10, 0x08, 0x7C, 0x00, 0x7C, 0x14, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x14, 0x1C, 0x00, 0x7C, 0x14, 0x6C, 0x00, 0x7C, 0x54, 0x44, 0x00, 0x5C, 0x54, 0x74, 0x00, 0x5C, 0x54, 0x74, 0x00, 0x7C, 0x40, 0x7C, 0x00, 0x7C, 0x14, 0x6C, 0x00, 0x7C, 0x54, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x05, 0x07, 0x00, 0x3E, 0x41, 0x41, 0x41, 0x22, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 0xBF, 0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7C, 0x10, 0x7C, 0x00, 0x3C, 0x40, 0x3C, 0x00, 0x7C, 0x08, 0x10, 0x08, 0x7C, 0x00, 0x7C, 0x00, 0x7C, 0x44, 0x38, 0x00, 0x7C, 0x00, 0x04, 0x7C, 0x04, 0x00, 0x0C, 0x70, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x3E, 0x02, 0x00, 0x3E, 0x00, 0x3E, 0x04, 0x08, 0x04, 0x3E, 0x00, 0x3E, 0x2A, 0x22, 0x00, 0x20, 0x10, 0x08, 0x04, 0x02, 0x00, 0x3E, 0x22, 0x1C, 0x00, 0x3C, 0x0A, 0x3C, 0x00, 0x02, 0x3E, 0x02, 0x00, 0x3E, 0x2A, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x42, 0x25, 0x12, 0x08, 0x24, 0x52, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x08, 0x08, 0x00, 0x00, 0x00
};

static const struct sPopupText PopupText[] PROGMEM =
{
	{NULL, 0},
	{M1, 1},
	{M2, 1},
	{M3, 1},
	{M4, 1},
	{M5, 1},
	{M6, 1},
	{M7, 1},
	{M8, 0},
	{M9, 1},
	{M10, 1},
	{M11, 0},
	{M12, 1},
	{M13, 1},
};

sDisplay_vars dataOutputVars = 
{
	.dateTime = {
		.second = 0,
		.minute = 0,
		.hour = 0,
		.DoW = 0,
		.day = 0,
		.month = 0,
		.year = 0
	},
	.headerStatus = H_BATAREY_0 | MEMORY_UNMOUNTED,
	.Temperature = NAN,
	.Pressure = NAN,
	.Humidity = NAN,
	.popUp = POPUP_NONE,
	.ADC_RAWVal = 0,
	.dbgStr = message
};  // �� ���� ������ �������� ��������

const char TemperatureFormat[] = TEMPERATURE_FORMAT;
const char PressureFormat[] = PRESSURE_PORMAT;
const char HumidityFormat[] = HUMIDITY_FORMAT;

static inline void read_displayBackground(const uint16_t offset, uint8_t* dest,  const uint16_t size)
{
#if DISPLAY_BACKGROUNG_STORE_EEPROM == 1
	eeprom_read_block(dest, disply_background + offset, size);
#else
	memcpy_P(dest, disply_background + offset, size);
#endif
}

static char* _printfn(char *buf, uint8_t N)
{
	for (uint8_t i = 0; i < N; ++i, ++buf)
		GLCD_Putchar(*buf);
	return buf;
}

static void printHorisontalBorder(uint8_t popup_width)
{
	for(uint8_t c = 1; c < popup_width - 1; ++c)
		GLCD_Putchar(SYMBOL_HORISONTAL_DOUBLE_LINES_CONER);
}

static void printHorisontalLine(uint8_t popup_width)
{
	GLCD_Putchar(SYMBOL_VERTICAL_DOUBLE_LINES_CONER);
	for(uint8_t c = 1; c < popup_width - 1; ++c)
		GLCD_Putchar(' ');
	GLCD_Putchar(SYMBOL_VERTICAL_DOUBLE_LINES_CONER);
}

static void resetXPos(uint8_t popup_width)
{
	dispx = LCD_PAGE_COLUMS / 2 - (popup_width * Font_5x7.u8Width) / 2 ;
}

// ������ ������ ����� � �������� �������� ������� ��������
static void PreparePopupBackground(uint8_t popup_width, uint8_t popup_height)
{
	// ����� �� �����
	popup_width += 4; //2 �������, 2 '||'
	popup_height += 2; // 2 x ====
	
	popup_width += popup_width / 9;
	
	font = &Font_5x7;
	uint8_t oldinterval = char2charInterval; // save interval
	char2charInterval = 0;
	
	dispPage = LCD_PAGES_COUNT / 2 - popup_height / 2 + 1;
	resetXPos(popup_width);
	GLCD_Putchar(SYMBOL_LEFT_UP_DOUBLE_LINES_CONER);
	printHorisontalBorder(popup_width);
	GLCD_Putchar(SYMBOL_RIGHT_UP_DOUBLE_LINES_CONER);
	
	++dispPage;
	resetXPos(popup_width);
	for (uint8_t page = 1; page < popup_height - 1; ++page, ++dispPage)
	{
		resetXPos(popup_width);
		printHorisontalLine(popup_width);
	}
	
	resetXPos(popup_width);
	GLCD_Putchar(SYMBOL_LEFT_DOWN_DOUBLE_LINES_CONER);
	printHorisontalBorder(popup_width);
	GLCD_Putchar(SYMBOL_RIGHT_DOWN_DOUBLE_LINES_CONER);

	char2charInterval = oldinterval; //restore interval;
}

static void drawSnowFlake(uint8_t column, uint8_t page)
{
	static uint8_t Snowflake_fase = 0;
	Snowflake_fase = ++Snowflake_fase % 8;
	
	font = &FONT_SPECIAL_SYMBOLS;
	
	lcd_goto(column, page);
	
	GLCD_Putchar(SNOWFLAKE_1 + Snowflake_fase);
}

static void drawPopupText(uint8_t column, uint8_t page, uint8_t* text)
{
	lcd_goto(column, page);
	while (1)
	{
		uint8_t c = *text++;
		if (c == '\0')
			return;
		if (c == '[')
		{
			LCD_Negative = 1;
			continue;
		}
		if (c == ']')
		{
			LCD_Negative = 0;
			continue;
		}
		if (c == '\n')
		{
			dispx = column;
			++dispPage;
			continue;
		}
		GLCD_Putchar(c);
	}
}

static void Display_on()
{
	if (GLCD_on(1))
		prepareScreen();
}

static int8_t read_displayOffCounter()
{
	return
#if DOUBLE_REFRASH_RATE == 1
	((int8_t)eeprom_read_byte(&settings.DisplayOffDelay)) * 2;
#else
	(int8_t)eeprom_read_byte(&settings.DisplayOffDelay);
#endif
}

#if STORE_DISPLAY_OFF_COUNTER_NVRAM == 0
static uint8_t DisplayAutoOffCounter = 0;
#endif


uint8_t checkDisplayEnabled()
{
#if STORE_DISPLAY_OFF_COUNTER_NVRAM == 1
	int8_t DisplayAutoOffCounter;
	READ_NVRAM(NVRAM_DISPLAYSTATUS_OFFSET, DisplayAutoOffCounter);
#endif
	
	uint8_t res;
	if (forceDisplayRefrash)
	{
		// force �������� ��������� ��������
		DisplayAutoOffCounter = read_displayOffCounter();
		Display_on();
		res = 1;
	}
	else
	{
		if ((USB_Status == _USB_ENABLED) &&
			(!eeprom_read_byte(&settings.isDisplayOFFIfUSBPowered))
			)
			DisplayAutoOffCounter = read_displayOffCounter();
	
		if (DisplayAutoOffCounter)
		{
			if (DisplayAutoOffCounter > 0)
				--DisplayAutoOffCounter;
			Display_on();
			res = 1;
		}
		else
		{
			GLCD_on(0); // dataOutputVars.DisplayAutoOffCounter == 0 - ���������
			res =  0;
		}
	}
	
#if STORE_DISPLAY_OFF_COUNTER_NVRAM == 1	
	SAVE_NVRAM(NVRAM_DISPLAYSTATUS_OFFSET, DisplayAutoOffCounter);
#endif

	return res;
}

static void prepareSelectIntervalMenu(uint8_t *string)
{
	string += 17; //������
	char buf[20];
	
	uint8_t selected = eeprom_read_byte(&settings.WriteIntervalVariants.selectd);
	for (uint8_t i = 0; i < sizeof(struct asist1) / sizeof(uint16_t); ++i)
	{
		uint16_t val = eeprom_read_word(&settings.WriteIntervalVariants.variants[i]);
		if (i == selected)
			sprintf_P(buf, PSTR("  [ % 5i sec. ]\n"), val);
		else
			sprintf_P(buf, PSTR("   % 5i sec. \n"), val);
		strcpy(string, buf);
		string += strlen(buf);
	}
	string[strlen(string) - 1] = '\0';
}

static void DrawMesures()
{
#if DRAW_MESURE_ENABLED == 1
	char buff[30];

	sprintf(buff, TemperatureFormat, dataOutputVars.Temperature); // ��� +
	drawTemperature(buff);
	
	sprintf(buff, PressureFormat, dataOutputVars.Pressure);
	drawPreassure(buff);
	
	sprintf(buff, HumidityFormat, dataOutputVars.Humidity);
	drawHumidity(buff);
	
	sprintf(buff, "%02i%c%02i%02i.%02i%04i",
		dataOutputVars.dateTime.hour,
		dataOutputVars.dateTime.second % 2 ? ':' : ':' + 1,
		dataOutputVars.dateTime.minute,
		dataOutputVars.dateTime.day,
		dataOutputVars.dateTime.month,
		2000 + dataOutputVars.dateTime.year
		);
	drawDate(buff);
#endif
}

static void processPopup(const enum enPopUP curentPopup)
{
	static enum enPopUP lastPopup = POPUP_NONE;
	static signed char popupAutocloseCounter = 0;
	
	// �������� ������
	struct sPopupText curentPopupText_s;
	memcpy_P(&curentPopupText_s, &PopupText[curentPopup], sizeof(struct sPopupText));
	uint8_t curentPopupText[strlen_P(curentPopupText_s.text) + 1];
	strcpy_P(curentPopupText, curentPopupText_s.text);
	if (curentPopup == POPUP_SELECT_SAVE_INTERVAL)
		prepareSelectIntervalMenu(curentPopupText);
	
	// ��������� ������ ������  � ������� ��������
	uint8_t popup_width = 0; // ������ ������ (��������)
	uint8_t popup_height = 1; // ������ ������ (��������)
	{
		char* str = (char*)curentPopupText;
		uint8_t curentLen = 0;
		uint8_t _char;
		while ((_char = *str++) != 0)
		{
			if ((_char == '[') || (_char == ']'))
				continue;
			if (_char == '\n')
			{
				if (curentLen > popup_width)
				popup_width = curentLen;
				curentLen = 0;
				++popup_height;
			}
			else
				if (_char == '\t')
					curentLen += 4;
			else
				++curentLen;
		}
		if (curentLen > popup_width)
			popup_width = curentLen;
	}
	
	if (curentPopupText_s.Snowflake_enabled)
		popup_width += 5; // ������ ������ �� 4 �����, ���� ��������� ��������
	
	// ������� ��������
	uint8_t Snowflake_column = LCD_PAGE_COLUMS / 2 - popup_width / 2 * (Font_5x7.u8Width + 1) + Font_5x7.u8Width / 2;
	uint8_t Snowflake_Page = 4;
	if (!curentPopupText_s.Snowflake_enabled)
		Snowflake_column -= 19; // ������� - ������ ��������
	
	if(curentPopup == lastPopup)
	{
		if (!forceDisplayRefrash)
		{
			if (--popupAutocloseCounter < 0)
			{
				dataOutputVars.popUp = POPUP_NONE;
				lastPopup = POPUP_NONE;
				prepareScreen();
				UpdateCallbacks(UI_DEFAULT);
				DrawMesures();
			}
		}
		else
			if (curentPopup == POPUP_SELECT_SAVE_INTERVAL)
			{
				popupAutocloseCounter = eeprom_read_byte(&settings.DisplayPopupVisableTime);
				drawPopupText(Snowflake_column + 19,
				LCD_PAGES_COUNT / 2 - popup_height / 2 + 1, curentPopupText);
			}
		
		// �������� ��������
		if (curentPopupText_s.Snowflake_enabled && (!forceDisplayRefrash) && !(popupAutocloseCounter < 0))
			drawSnowFlake(Snowflake_column, Snowflake_Page);
	}
	else
	{
		// ����� �����, ������ ���
		PreparePopupBackground(popup_width, popup_height); // ���
		drawPopupText(Snowflake_column + 19,
		LCD_PAGES_COUNT / 2 - popup_height / 2 + 1, curentPopupText);
		if (curentPopupText_s.Snowflake_enabled)
			drawSnowFlake(Snowflake_column, Snowflake_Page);
		
		lastPopup = curentPopup;
		popupAutocloseCounter = eeprom_read_byte(&settings.DisplayPopupVisableTime);
	}
}

void RefrashDisplay()
{
	if (!checkDisplayEnabled())
	{
		UpdateCallbacks(UI_SLEEP);
		MesureEnableVoteing(DiSPLAY_VOTE, 0); // �������� ��������, ���� ����� �����
		*dataOutputVars.dbgStr = '\0'; // ������� ���������
		return; //������� ��������, ������� ��� ���������
	}
	
	MesureEnableVoteing(DiSPLAY_VOTE, 1); // �������� ��������, ���� ����� �����
			
	if (dataOutputVars.popUp > POPUP_MEMORY_FULL)
		dataOutputVars.popUp = POPUP_NONE;
		
	// ����� ��� ���������?
	if (dataOutputVars.popUp)
		processPopup(dataOutputVars.popUp);	
	else
		// ��� ������, ������ ���������
		DrawMesures();
		
	drawHeader();
	
	// [DEBUG]
	font = &Font_5x7;

	lcd_goto(0, 0);
	lcd_puts(dataOutputVars.dbgStr);
	
	//dataOutputVars.dbgStr[0] = '\0';
	// [/DEBUG]
}

void drawHeader()
{
	uint8_t MemFilled = (dataOutputVars.headerStatus & MEMORY_FILL_MASK_VAL) >> 8;
	
	font = &FONT_SPECIAL_SYMBOLS;
	lcd_goto(8, 0);
		
	if ((*dataOutputVars.dbgStr == '\0') && !(dataOutputVars.headerStatus & MEMORY_UNMOUNTED))
	{
		GLCD_Putchar(MEMORY_COUNTER_START);
		for (uint8_t i = 0; i < 15; ++i)
			if (MemFilled)
			{
				--MemFilled;
				GLCD_Putchar(MEMORY_SEGMENT_FILLED);
			}
			else
				GLCD_Putchar(MEMORY_SEGMENT_EMPTY);
		GLCD_Putchar(MEMORY_COUNTER_END);
	}
	else
		lcd_goto(8 + 4 * (15 + 2), 0);
	
	dispx += 2 + 4;
	
	GLCD_Putchar(dataOutputVars.headerStatus & H_RECORDING ? RECORDING_SYMBOL : NO_RECORDING_SYMBOL);

	dispx += 2;
	
	GLCD_Putchar(dataOutputVars.headerStatus & H_BLUETOOTH ? BLUETOOTH : NO_BLUETOOTH);
		
	dispx += 2;	
	
	uint8_t chargeStatus = dataOutputVars.headerStatus & H_BAT_MASK_VAL;
	
	GLCD_Putchar(BATAREY_START);
#if 0
	if (chargeStatus == H_BATAREY_CHARGE)
	{
		GLCD_Putchar(BATAREY_FILLED_SEGMENT);
		if (dataOutputVars.dateTime.second % 2)
			GLCD_Putchar(BATARY_CHARGE_SEGMENT);
		else
		{
			GLCD_Putchar(BATARY_EMPTY_SEGMENT);
			GLCD_Putchar(BATARY_EMPTY_SEGMENT);
		}
		GLCD_Putchar(BATARY_EMPTY_SEGMENT);
	}
	else
	{
		for (int i = 0; i < 4; ++i)
			if (chargeStatus)
			{
				chargeStatus--;
				GLCD_Putchar(BATAREY_FILLED_SEGMENT);
			}
			else
				GLCD_Putchar(BATARY_EMPTY_SEGMENT);
	}
#else
	for (int i = 0; i < 4; ++i)
		if (
			(i == 1) && 
			(dataOutputVars.headerStatus & H_BATAREY_CHARGE) && 
			(dataOutputVars.dateTime.second % 2)
			)
		{
			GLCD_Putchar(BATARY_CHARGE_SEGMENT); // ��� �������� 2 ������
			++i;
			chargeStatus -= 2;
		}
		else
		{
			if (chargeStatus & H_BAT_MASK_VAL)
			{
				chargeStatus--;
				GLCD_Putchar(BATAREY_FILLED_SEGMENT);
			}
			else
				GLCD_Putchar(BATARY_EMPTY_SEGMENT);
		}
#endif		
	GLCD_Putchar(BATARY_END);
};

void prepareScreen()
{
	for (uint8_t Quater_page = 0; Quater_page < LCD_PAGES_COUNT * 4; ++Quater_page)
	{
		uint8_t buf[LCD_PAGE_COLUMS / 4 + 1];
		read_displayBackground(Quater_page * LCD_PAGE_COLUMS / 4, buf, LCD_PAGE_COLUMS / 4);
		lcd_goto_x_page((Quater_page % 4) * (LCD_PAGE_COLUMS / 4), Quater_page >> 2);
		LCD_start_Data_transfer();
		for (uint16_t column = 0; column < LCD_PAGE_COLUMS / 4; ++column)
		{
			uint8_t data = buf[column];
			if (LCD_Negative)
				data = ~data;
			LcdDataWrite(data);
		}
		i2c_stop();
	}
	/*
	// ��������
	lcd_goto_x_page(LCD_PAGE_COLUMS - 1, LCD_PAGES_COUNT - 1);
	LCD_start_Data_transfer();
	LcdDataWrite(LCD_Negative ? 0xff : 0);
	i2c_stop();
	//*/
}

void drawTemperature(char* buf)
{
	font = &Font_Digits_big;
	
	lcd_goto(TEMPERATURE_POSITION_X, TEMPERATURE_POSITION_Y);
	buf = _printfn(buf, 4);
	
	font = &Font_Digits_small;
	//dispx += 5;
	dispPage++;
	lcd_puts(buf);
}

void drawPreassure(char* buf)
{
	font = &Font_Digits_big;
	lcd_goto(PRESSURE_CENTER_POSITION_X - strlen(buf) / 2 * 9, PRESSURE_POSITION_Y);
	lcd_puts(buf);
	
	font = &Font_5x7;
	uint8_t x = ++dispx;	
	lcd_putsp(PSTR("mm"));
	dispx = x;
	++dispPage;
	lcd_putsp(PSTR("Hg"));
}

void drawHumidity(char *buf)
{
	font = &Font_Digits_big;
	lcd_goto(HUMIDITY_POSITION_X, HUMIDITY_POSITION_Y);
	buf = _printfn(buf, 4);
	
	font = &Font_Digits_small;
	//dispx += 5;
	dispPage++;
	lcd_puts(buf);
}

void drawDate(char *buf)
{
	lcd_goto(DATE_TIME_POSITION_X, DATE_POSITION_Y);
	font = &Font_Digits_big;
	buf = _printfn(buf, 2);
		
	font = &FONT_SPECIAL_SYMBOLS;
	buf = _printfn(buf, 1);
	
	font = &Font_Digits_big;
	buf = _printfn(buf, 2);
	
	font = &Font_Digits_small;
	lcd_goto(DATE_DATE_POSITION_X, DATE_POSITION_Y);
	buf = _printfn(buf, 5);
	
	lcd_goto(DATE_DATE_POSITION_X + 2, DATE_POSITION_Y + 1);
	buf = _printfn(buf, 4);
}

uint8_t updateUsedSpaceIndicator(float precent)
{
	/*
	100 %			-  15
	---------------   ---
	 precent		-  x
	 
	 x = 16 * precent / 100
	*/
	
	if (isnan(precent))
		dataOutputVars.headerStatus |= MEMORY_UNMOUNTED;
	else
	{
		uint32_t used = lround(0x0f * precent / 100.0);
		dataOutputVars.headerStatus = (dataOutputVars.headerStatus & ~(MEMORY_FILL_MASK)) | (used << 8);
	}
}
