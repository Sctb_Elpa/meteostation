/*
 * Counters.c
 *
 * Created: 16.05.2014 15:28:33
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <math.h>
#include <stdlib.h>
#include <avr/sleep.h>

#include "config/AppConfig.h"

#include "filter.h"
#include "Settings.h"

#include "Counters.h"

#define STANDART_MESURE_TIME	(0.9) // ��������� ����� ���������
#define START_FREQ				(1000.0) // ��������� ������� (���� ������ �� �����)

#define NIDDLE_FILTER_FACTOR	10

/************************************************************************/
/* T0 8bit - ������� ������� �����������								*/
/* T1 16bit - ������� ������� ��������									*/ 
/* T3 16bit - ������� ������� �������									*/                                                  
/************************************************************************/

union u16bitCounter
{
	struct 
	{
		uint16_t REGISTER;
		uint16_t Extantion;
	} s;	
	uint32_t value;
};

// ���������� �������� ������� �������
uint16_t RefFreqTimer_Ovf;

union u8bitCounter
{
	struct  
	{
		uint8_t REGISTER;
		uint32_t Extantion:24;
	} s;
	uint32_t value;
};

static union u16bitCounter	PresureFreqCounter_Work,	// ����� � ������
							PresureFreqCounter_Target,  // ��� ���������� ����������
							PresureFreqCounter_Target_was,
							PresureFreqCounter_Target_new;
							
static union u8bitCounter	TemperatureFreqCounter_Work,	// ����� � ������
							TemperatureFreqCounter_Target,  // ��� ���������� ����������
							TemperatureFreqCounter_Target_was,
							TemperatureFreqCounter_Target_new;
									
static union u16bitCounter  PresureMesureValue_Start,  // �������� �������� ������� ������� � ������ ����� (��� P)
							PresureMesureValue_Result,  // ��������� (P)
							TemperatureMesureValue_Start,  // �������� �������� ������� ������� � ������ ����� (��� T)
							TemperatureMesureValue_Result;  // ��������� (T)
#if FILTER_ENABLED == 1
	float prevdiffP;
	float prevdiffT;
#endif

// ��������� �����, �� �������� ��������� �������, ���� �� ���� �������� �������
// AproxFreq, �� ���� ��������� STANDART_MESURE_TIME
static uint32_t calcTargetvalue(float F)
{
	uint32_t res = (uint32_t)lround(F * STANDART_MESURE_TIME);
	return res ? res : 1;
}

static float antiNiddleFilter(float curent, float prev, float *prevdiff)
{
#if FILTER_ENABLED == 1
	if (isnan(*prevdiff))
	{
		*prevdiff = 0;
		return curent;		
	}
	else
	{
		float res;
		if  (
			((*prevdiff > 0) && (fabs(curent - prev) > (*prevdiff) * NIDDLE_FILTER_FACTOR)) ||
			((*prevdiff <= 0) && (fabs(curent - prev) > NIDDLE_FILTER_FACTOR / 10))
			)
			{
				res = prev;
				if (*prevdiff <= 0)
				{
					*prevdiff = NAN;
					return res;
				}
			}
			else
				res = curent;
		*prevdiff = fabs(res - prev);
		return res;
	}
#else
	return curent;
#endif
}

// ��������� �������� ����������� � �������� �� ��������
void calcFsensOutputVals(float* TemperatureResult, float* PresureResult)
{
	// ������� �����������:
	// TemperatureMesureValue_Result - ���������� ��������� ������� F_CPU �� �����, ������� ������������ �� ������ TemperatureFreqCounter_Target ���������
	// ���������� �������
	float Ft =  F_CPU * (float)TemperatureFreqCounter_Target_was.value / (float)TemperatureMesureValue_Result.value;
	
	//�������� ����������
	struct sTemperatureCoeffs TemperatureCoeffs;
	eeprom_read_block(&TemperatureCoeffs, &settings.TemperatureCoeffs, sizeof(struct sTemperatureCoeffs));
	
	float TempF_minus_Ft0 = Ft - TemperatureCoeffs.F0;
	float _temp = TempF_minus_Ft0;	
	
	float Temperature_fSens = TemperatureCoeffs.T0;
	for (unsigned char i = 0; i < 3; ++i, _temp *= TempF_minus_Ft0)
		Temperature_fSens += TemperatureCoeffs.C[i] * _temp;
	
	*TemperatureResult = antiNiddleFilter(Temperature_fSens, *TemperatureResult, &prevdiffT);
	if (*TemperatureResult == Temperature_fSens)
		TemperatureFreqCounter_Target_new.value = calcTargetvalue(Ft);

	////////////////////////////////////////////////////////////////////	
			
	// ������� ��������:
	// PresureMesureValue_Result - ���������� ��������� ������� F_CPU �� �����, ������� ������������ �� ������ PresureFreqCounter_Target ���������
	// ���������� �������
	float Fp =  F_CPU * (float)PresureFreqCounter_Target.value / (float)PresureMesureValue_Result.value;
			
	//�������� ����������
	struct sPressureCoeffs PressureCoeffs;
	eeprom_read_block(&PressureCoeffs, &settings.PressureCoeffs, sizeof(struct sPressureCoeffs));
	
	float Ft_minus_Ft0 = Ft - PressureCoeffs.Ft0;
	float PresF_minus_Fp0 = Fp - PressureCoeffs.Fp0;
		
	float Pressure_fSens = PressureCoeffs.A[0] + PressureCoeffs.A[5] * Ft_minus_Ft0 * PresF_minus_Fp0;
	for (unsigned char i = 1; i < 3; ++i, Ft_minus_Ft0 *= Ft_minus_Ft0, PresF_minus_Fp0 *= PresF_minus_Fp0)
		Pressure_fSens += PressureCoeffs.A[i] * Ft_minus_Ft0 +  PressureCoeffs.A[i + 2] * PresF_minus_Fp0;

	*PresureResult = antiNiddleFilter(Pressure_fSens, *PresureResult, &prevdiffP);
	if (*PresureResult == Pressure_fSens)
		PresureFreqCounter_Target_new.value = calcTargetvalue(Fp);
}

void EnableCounters(uint8_t enable)
{
#if SLEEP_GENERATORS == 1
	GENERATORS_CTRL_DDR |= 1 << GENERATORS_CTRL_PIN_N;
#endif
	if (enable)
	{
		sleep_disable(); // ����� ������!
		
		// ����� ��� ��������?
		// �������� ���������?
		if	(
			(TCCR0B == ((1 << CS02) | (1 << CS01) | (1 << CS00))) &&
			(TCCR1B == ((1 << CS12) | (1 << CS11) | (1 << CS10))) &&
			(TCCR3B == (1 << CS30))
			)
			return;

		// ����� �� power_down
		PRR0 &= ~((1 << PRTIM0) | (1 << PRTIM1));
		PRR1 &= ~(1 << PRTIM3);
		
#if SLEEP_GENERATORS == 1		
		GENERATORS_CTRL_PORT |= 1 << GENERATORS_CTRL_PIN_N;
#endif
		// ��������� ��������
		TemperatureMesureValue_Start.value = 0;
		PresureMesureValue_Start.value = 0;
		
		TemperatureFreqCounter_Target.value = 
			TemperatureFreqCounter_Target_was.value = 
			TemperatureFreqCounter_Target_new.value = 
			PresureFreqCounter_Target.value =
			PresureFreqCounter_Target_was.value =
			PresureFreqCounter_Target_new.value = calcTargetvalue(START_FREQ);
		
		// �������� ������� ������� �������
		TCCR3A = 0;			 // no output
		TCCR3B = 1 << CS30;	 // Fin = F_CPU
		TIMSK3 = 1 << TOIE3; // ��������� ���������� �� ������������
		
		// �������� ������� ������� �����������
		DDRD &= ~(1 << 7);
		TCCR0A = 0;			// no output
		TCCR0B = (1 << CS02) | (1 << CS01) | (1 << CS00); // rising age
		TIMSK0 = 1 << TOIE0; // ��������� ���������� �� ������������
		
		// �������� ������� ������� ��������
		DDRD &= ~(1 << 6);
		TCCR1A = 0;			// no output
		TCCR1B = (1 << CS12) | (1 << CS11) | (1 << CS10); // rising age
		TIMSK1 = 1 << TOIE1; // ��������� ���������� �� ������������
		
		// "�������" ��������
		TCNT0 = 0xff;
		TCNT1 = 0xffff;

		// � ����� ������� ��� ����������� ������ � ����������
#if FILTER_ENABLED == 1
		float revdiffP = NAN;
		float revdiffT = NAN;
#endif
	}
	else
	{
		// ��������� ����� ���������
		TCCR0B = 0;
		TCCR1B = 0;
		TCCR3B = 0;
		
		// power down
		PRR0 |= (1 << PRTIM0) | (1 << PRTIM1);
		PRR1 |= (1 << PRTIM3);
#if SLEEP_GENERATORS == 1
		GENERATORS_CTRL_PORT &= ~(1 << GENERATORS_CTRL_PIN_N);
#endif	
		sleep_enable(); // ����� �����
	}
}

// ���������� �����������
ISR(TIMER0_OVF_vect)
{
	// ������ �������� �������� ������� �������
	union u16bitCounter curentVal;
	curentVal.s.REGISTER = TCNT3;
	curentVal.s.Extantion = RefFreqTimer_Ovf;
	
	// ������� ������ ������ �����
	if (TemperatureMesureValue_Start.value == 0)
	{
		// ����������� ����� ������
		TemperatureMesureValue_Start.value = curentVal.value;
		
		TemperatureFreqCounter_Work.value = TemperatureFreqCounter_Target.value; // ������������ ��������
	}
	
	if (TemperatureFreqCounter_Work.s.Extantion)
		--TemperatureFreqCounter_Work.s.Extantion; // - 1 ����������
	else
	{
		if (TemperatureFreqCounter_Work.s.REGISTER)
		{ // ��������� ������ 1 ������������
			if (TCNT0 >= TemperatureFreqCounter_Work.s.REGISTER)
			{ // ���� ��� ����� ��������, ������� ��� �������� ������ ��� ����� -> ����� �����
			  // �������� ������� �������� �������� ������� �������
				curentVal.s.REGISTER = TCNT3;
				curentVal.s.Extantion = RefFreqTimer_Ovf;
				goto __end_cycleT0;
			}
			// ��������� �������
			TCNT0 -= TemperatureFreqCounter_Work.s.REGISTER;
			TemperatureFreqCounter_Work.s.REGISTER = 0;
		}
		else
		{
__end_cycleT0:
			// ��� ���������
			TemperatureMesureValue_Result.value = curentVal.value - TemperatureMesureValue_Start.value;
			TemperatureMesureValue_Start.value = 0;
			TemperatureFreqCounter_Target_was.value = TemperatureFreqCounter_Target.value;
			TemperatureFreqCounter_Target.value = TemperatureFreqCounter_Target_new.value; // update mesure time
			
			TCNT0 = 0xff; // ��������� ������� ���������� ������� �������� ����� ����
		}
	}
}

// ���������� ��������
ISR(TIMER1_OVF_vect)
{
	// ������ �������� �������� ������� �������
	union u16bitCounter curentVal;
	curentVal.s.REGISTER = TCNT3;
	curentVal.s.Extantion = RefFreqTimer_Ovf;
	
	// ������� ������ ������ �����
	if (PresureMesureValue_Start.value == 0)
	{
		// ����������� ����� ������
		PresureMesureValue_Start.value = curentVal.value;
		
		PresureFreqCounter_Work.value = PresureFreqCounter_Target.value; // ������������ ��������
	}
	
	if (PresureFreqCounter_Work.s.Extantion)
		--PresureFreqCounter_Work.s.Extantion; // - 1 ����������
	else
	{
		if (PresureFreqCounter_Work.s.REGISTER)
		{ // ��������� ������ 1 ������������
			if (TCNT1 >= PresureFreqCounter_Work.s.REGISTER)
			{ // ���� ��� ����� ��������, ������� ��� �������� ������ ��� ����� -> ����� �����
				// �������� ������� �������� �������� ������� �������
				curentVal.s.REGISTER = TCNT3;
				curentVal.s.Extantion = RefFreqTimer_Ovf;
				goto __end_cycleT1;
			}
			// ��������� �������
			TCNT1 -= PresureFreqCounter_Work.s.REGISTER;
			PresureFreqCounter_Work.s.REGISTER = 0;
		}
		else
		{
__end_cycleT1:
			// ��� ���������
			PresureMesureValue_Result.value = curentVal.value - PresureMesureValue_Start.value;
			PresureMesureValue_Start.value = 0;
			PresureFreqCounter_Target_was.value = PresureFreqCounter_Target.value;
			PresureFreqCounter_Target.value = PresureFreqCounter_Target_new.value; // update mesure time
			
			TCNT1 = 0xffff; // ��������� ������� ���������� ������� �������� ����� ����
		}
	}
}

// ���������� ������� �������
ISR(TIMER3_OVF_vect)
{
	++RefFreqTimer_Ovf;
}