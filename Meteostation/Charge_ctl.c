/*
 * Charge_ctl.c
 *
 * Created: 16.05.2014 13:04:35
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <math.h>

#include <LUFA/Drivers/Peripheral/ADC.h>

#include "debug.h"
#include "Display.h"

#include "Charge_ctl.h"

#define CHARGE_ACTIVE_LVL	0
#define CHARGE_PIN_N		6
#define CHARGE_DDR			DDRE
#define CHARGE_PIN			PINE
#define CHARGE_PORT			PORTE


static uint16_t ADC_getRawValue()
{
#if PWR_REDUCTION_CTRL == 1
	PRR0 &= ~(1 << PRADC);
#endif
	_delay_us(10); // �� ������..
	
	// Initialize the ADC driver before first use
	ADC_Init(ADC_SINGLE_CONVERSION | ADC_PRESCALE_32);
	// Must setup the ADC channel to read beforehand
	ADC_SetupChannel(BATARY_VOLTAGE_CHANEL);
	// Perform a single conversion of the ADC channel 1
	
	_delay_ms(1); // �� ������..
	
	uint16_t res = ADC_GetChannelReading(ADC_REFERENCE_INT2560MV | ADC_RIGHT_ADJUSTED | ADC_CHANNEL0);
	ADC_Disable();
#if PWR_REDUCTION_CTRL == 1
	PRR0 |= 1 << PRADC; // ADC clock disabled
#endif
	return res;
}

uint8_t ADC_getChargeLevel(uint16_t rawWal)
{
#if 1
	uint8_t res = lround((rawWal - CHARGE_0) * 100.0 / (float)(CHARGE_100 - CHARGE_0));
	return res > 100 ? 100 : (res < 0 ? 0 : res);
#else
	return rawWal >> 2;
#endif
}

void initCharge_Ctrl()
{
	CHARGE_DDR &= ~(1 << CHARGE_PIN);
	//CHARGE_PORT |= 1 << CHARGE_PIN; // ��������
#if PWR_REDUCTION_CTRL == 1
	PRR0 |= 1 << PRADC; // ADC clock disabled
#endif
}

uint8_t isCharge()
{
	uint8_t res;
#if CHARGE_ACTIVE_LVL == 0
	res = (~CHARGE_PIN) & (1 << CHARGE_PIN_N);
#else
	res =  CHARGE_PIN & (1 << CHARGE_PIN_N);
#endif
	return res;
}

void refreshBataryStatus()
{
	dataOutputVars.ADC_RAWVal = ADC_getRawValue();
	//DEBUG_MSG("CHG: %i", ADC_getChargeLevel(dataOutputVars.ADC_RAWVal));
	enum enHeaderStatus header = dataOutputVars.headerStatus & (~H_BAT_MASK);
	
	if (isCharge())
		header |= H_BATAREY_CHARGE;
	uint8_t N = lround(ADC_getChargeLevel(dataOutputVars.ADC_RAWVal) / 25.0);
	header |= N > H_BATAREY_4 ? H_BATAREY_4	: N;
	
	dataOutputVars.headerStatus = header;
}