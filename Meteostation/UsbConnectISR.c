/*
 * UsbConnectISR.c
 *
 * Created: 15.05.2014 15:24:43
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include "Display.h"

#include "UsbConnectISR.h"

enum USB_STATE USB_Status = USB_CHANGE_STATUS_REQ | USB_STARTUP;

void USBConnectTrace_Enable(char enable)
{
	USB_CON_SIG_DDR &= ~(1 << USB_CON_SIG_PIN_N); //input
	if (enable)
	{
		USB_CON_SIG_PORT |= 1 << USB_CON_SIG_PIN_N; //pull up
		//any age
		EICRB |= 1 << ISC70;
		EICRB &= ~(1 << ISC71);
		
		EIMSK |= 1<< USB_CON_SIG_PIN_N;
	}
	else
		EIMSK &= ~(1<<USB_CON_SIG_PIN_N);
}

uint8_t isUSBConnected()
{
#if USB_CON_SIG_ACTIVE_LVL == 0
	return ~USB_CON_SIG_PIN & (1 << USB_CON_SIG_PIN_N);
#else
	USB_CON_SIG_PIN & (1 << USB_CON_SIG_PIN_N);
#endif
}

ISR(INT7_vect)
{
	EIFR |= 1 << USB_CON_SIG_PIN_N; //����� �����
	USB_Status = USB_CHANGE_STATUS_REQ;
}