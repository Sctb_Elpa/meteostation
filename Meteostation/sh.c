/*
  arsh - arsh.c
  Copyright (C) 2009 Bert Vermeulen

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// this file is my modification

#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>

#include "Lib/FatFs/ff.h"
#include "Lib/uart.h"
#include "BT.h"
#include "FileIO.h"
#include "Charge_ctl.h"
#include "Display.h"
#include "Lib/FatFs/ff.h"
#include "Settings.h"

#include "debug.h"

#include "sh.h"

static const char t_test[] PROGMEM = "test";
static const char t_ls[] PROGMEM = "ls";
static const char t_dir[] PROGMEM = "dir";
static const char t_pwd[] PROGMEM = "pwd";
static const char t_cd[] PROGMEM = "cd";
static const char t_cat[] PROGMEM = "cat";
static const char t_df[] PROGMEM = "df";
static const char t_current[] PROGMEM = "current";
static const char t_take_mesure[] PROGMEM = "takemesure";
static const char t_date[] PROGMEM = "date";
static const char t_ping[] PROGMEM = "ping";
static const char t_recstart[] PROGMEM = "recstart";
static const char t_wvariants[] PROGMEM = "wvariants";
static const char t_wstop[] PROGMEM = "wstop";
static const char t_ATmAB[] PROGMEM = "AT-AB";

static const tokenstruct shelltokens[] PROGMEM = {
	{ TOK_TEST, t_test },
	{ TOK_LS, t_ls },
	{ TOK_LS, t_dir },
	{ TOK_PWD, t_pwd },
	{ TOK_CD, t_cd },
	{ TOK_CAT, t_cat },
	{ TOK_DF, t_df },
	{ TOK_CURRENT, t_current },
	{ TOK_TAKE_MESURE, t_take_mesure },
	{ TOK_DATE, t_date },
	{ TOK_PING, t_ping },
	{ TOK_RECSTART, t_recstart },
	{ TOK_GET_INTERVAL_VARIANTS, t_wvariants },
	{ TOK_STOP_RECORDING, t_wstop },	
	{ TOK_ATmAB, t_ATmAB },
	{ TOK_NULL, NULL }
};

////////////////////////////////////////////
static void cmd_ls(char* path)
{
	char buff[32];
	FRESULT res;
	
	DIR dir;
	FILINFO fno;
		
	res = f_opendir(&dir, path);
	if (res != FR_OK)
	{
		printf_P(PSTR("Open %s failed (%i)"), path, res);
		return;
	}
	else
	{
		for (;;)
		{
			res = f_readdir(&dir, &fno);
			if (res != FR_OK || fno.fname[0] == 0) 
				break;  /* Break on error or end of dir */
			if (!strcmp_P(fno.fname, PSTR(".")))
				continue;             /* Ignore dot entry */
			if (fno.fattrib & AM_DIR)
				sprintf_P(buff, PSTR("%s/"), fno.fname);
			else
				strcpy(buff, fno.fname);
#if USE_WDT == 1
			wdt_reset();
#endif
			uart_puts(buff);
			uart_puts_p(newLine);
		}
		f_closedir(&dir);
	}
}

static void cmd_pwd()
{
	char buff[32];
	FRESULT res = f_getcwd(buff, sizeof(buff));
	if (res)
		printf_P(PSTR("Error in pwd (%i)"), res);
	else
		uart_puts(buff);
}

static void cmd_cd(char * path)
{
	FRESULT res = f_chdir(path);
	if (res)
		printf_P(PSTR("Error in cd (%i)"), res);
	else
	{
		uart_puts_p(newLine);
		cmd_pwd();
	}
}

static void cmd_cat(char* filename)
{
	FIL f;
	FRESULT res = f_open(&f, filename, FA_READ);
	if (res)
	{
		printf_P(PSTR("Failed to open %s (%i)"), filename, res);
	}
	else
	{
		char buff[32];
		char* r;
		while(!f_eof(&f))
		{
			r = f_gets(buff, sizeof(buff), &f);
			if (r != buff)
			{
				printf_P(PSTR("\n\rRead error (%i)"), f_error(&f));
				break;
			}
			uart_puts(buff);
#if USE_WDT == 1
			wdt_reset();
#endif
		}
		f_close(&f);
	}
}

void cmd_df()
{
	uint32_t freeSectors;
	FRESULT res = getFreeSpace(&freeSectors, NULL);
	if (!res)
		printf_P(PSTR("Free: %i * %i bytes"), freeSectors, _MAX_SS);
	else
		printf_P(PSTR("Error get free (%i)"), res);
}

void cmd_curent()
{
	printf_P(saveStrFormat, 
		(int)dataOutputVars.dateTime.hour,
		(int)dataOutputVars.dateTime.minute,
		(int)dataOutputVars.dateTime.second,
		dataOutputVars.Temperature,
		dataOutputVars.Pressure,
		dataOutputVars.Humidity,
		(int)ADC_getChargeLevel(dataOutputVars.ADC_RAWVal));
}

void cmd_date(char * newdate)
{
	if (newdate)
	{
		uint8_t res = 0;
		int32_t year = dataOutputVars.dateTime.year; 
		int32_t month = dataOutputVars.dateTime.month; 
		int32_t day = dataOutputVars.dateTime.day; 
		int32_t hour = dataOutputVars.dateTime.hour; 
		int32_t minute = dataOutputVars.dateTime.minute; 
		int32_t second = dataOutputVars.dateTime.second; 
		
		uint8_t Len = strlen(newdate);
		switch(Len)
		{
			case 8: //��������
				res = sscanf_P(newdate, PSTR("%2d%2d%2d%2d"), 
							&month,
							&day,
							&hour,
							&minute
						) == 4;
				break;
			case 10: //����������
				res = sscanf_P(newdate, PSTR("%2d%2d%2d%2d%2d"),
								&month,
								&day,
								&hour,
								&minute,
								&year
							) == 5;
				break;
			case 11: //��������.��
				res = sscanf_P(newdate, PSTR("%2d%2d%2d%2d.%2d"),
								&month,
								&day,
								&hour,
								&minute,
								&second
							) == 5;
				break;
			case 13: //��������[��][.��]
				res = sscanf_P(newdate, PSTR("%2d%2d%2d%2d%2d.%2d"),
								&month,
								&day,
								&hour,
								&minute,
								&year,
								&second
							) == 6;
				break;
			default:
				break;
		}
		if (res)
		{
			struct ds1307DateTime newdate = {second, minute, hour, 0, day, month, year};
			dataOutputVars.dateTime = newdate;
			ds1307_setdate(&newdate);
			uart_puts_p(PSTR("Date updated\n\r"));
		}
		else
		{
			printf_P(PSTR("Usage: date [MMDDhhmm[YY][.ss]]\n\r>%s (L=%i, res=%i)"), newdate, Len, res);
			return;
		}
	}
	printf_P(PSTR("Now: %02i.%02i.%04i %02i:%02i:%02i"),
				dataOutputVars.dateTime.day,
				dataOutputVars.dateTime.month,
				(int)dataOutputVars.dateTime.year + 2000,
				dataOutputVars.dateTime.hour,
				dataOutputVars.dateTime.minute,
				dataOutputVars.dateTime.second);
}

void cmd_recStart(uint8_t variant, uint16_t newdelay)
{
	if (variant > 2)
	{
		printf_P(PSTR("Incorrect variant number: %i"), variant);
		return;
	}
	eeprom_update_byte(&settings.WriteIntervalVariants.selectd, variant);
	eeprom_update_word(&settings.WriteIntervalVariants.variants[variant], newdelay);
	if (enableSaveMode())
		uart_puts_p(PSTR("Saving started..."));
	else
		uart_puts_p(PSTR("Saving failed to start"));
}

///////////////////////////////////////////

void process_command(char* cmd)
{
	int16_t tokens[MAX_NUM_TOKENS + 1];
	parse_cmdline(cmd, tokens);
	
	switch (tokens[0])
	{
		case TOK_TEST:
			uart_puts_p(PSTR("test text\n\r"));
			if (tokens[1])
				uart_puts(cmd - tokens[1]);
			break;
		case TOK_LS:
			if (tokens[1])
				cmd_ls(cmd - tokens[1]);
			else
				cmd_ls(".");
			break;
		case TOK_PWD:
			cmd_pwd();
			break;
		case TOK_CD:
			if (tokens[1])
				cmd_cd(cmd - tokens[1]);
			else
				uart_puts_p(PSTR("Usage: cd <path>"));
			break;
		case TOK_CAT:
			if (tokens[1])
				cmd_cat(cmd - tokens[1]);
			else
				uart_puts_p(PSTR("Usage: cat <file>"));
			break;
		case TOK_DF:
			cmd_df();
			break;
		case TOK_CURRENT:
			cmd_curent();
			break;
		case TOK_TAKE_MESURE:
			if (SaveMesureNow(SAVE_REASON_FORCE))
				uart_puts_p(PSTR("Saved"));
			else
				uart_puts_p(PSTR("Save failed"));
			break;
		case TOK_DATE:
			if (tokens[1])
				cmd_date(cmd - tokens[1]);
			else
				cmd_date(NULL);
			break;
		case TOK_PING:
			uart_puts_p(PSTR("pong"));
			break;
		case TOK_GET_INTERVAL_VARIANTS:
			printf_P(PSTR("Write interval variants: %i;%i;%i, selected: %i"), 
				eeprom_read_word(&settings.WriteIntervalVariants.variants[0]),
				eeprom_read_word(&settings.WriteIntervalVariants.variants[1]),
				eeprom_read_word(&settings.WriteIntervalVariants.variants[2]),
				eeprom_read_byte(&settings.WriteIntervalVariants.selectd)
				);
			break;
		case TOK_RECSTART:
			{
				uint8_t variant = eeprom_read_byte(&settings.WriteIntervalVariants.selectd);
				uint16_t interval = eeprom_read_word(&settings.WriteIntervalVariants.variants[variant]);
				if (tokens[1])
				{
					variant = atoi(cmd - tokens[1]);
					if (tokens[2])
						interval = atoi(cmd - tokens[2]);
				}
				cmd_recStart(variant, interval);
			}
			break;
		case TOK_STOP_RECORDING:
			disableSaveMode();
			uart_puts_p(PSTR("Recording canceled"));
			break;
		case TOK_ATmAB:
			return;
		default:
			printf_P(PSTR("Unknown command: %s"), cmd);
			break;
	}
	uart_puts_p(newLine);
}

uint8_t tokenize(char *buf, int16_t *tokens, uint8_t* num_tokens)
{
	for(uint16_t i = 0;; i++)
	{
		uint8_t token = pgm_read_byte(&shelltokens[i].token);
		if (token == TOK_NULL)
			return 0;
		char *ptr;
		memcpy_P(&ptr, &shelltokens[i].keyword, sizeof(char*));
		if(!strcasecmp_P(buf, ptr))
		{
			tokens[(*num_tokens)++] = token;
			return 1;
		}
	}
}

void parse_cmdline(char* cmdbuf, int16_t *tokens)
{
	int i;
	char *word_start;

	// skip leading whitespace
	for(i = 0; cmdbuf[i] && cmdbuf[i] == ' '; i++)
		;
	word_start = NULL;
	uint8_t num_tokens = 0;
	while(cmdbuf[i] != 0 && num_tokens < MAX_NUM_TOKENS)
	{
		if(cmdbuf[i] != ' ')
			// got a word
			word_start = cmdbuf + i;
		while(cmdbuf[i] != 0 && cmdbuf[i] != ' ')
			// skip ahead to the next word
			i++;
		if(cmdbuf[i] == ' ')
			// delimit word
			cmdbuf[i++] = 0;
		if (!tokenize(word_start, tokens, &num_tokens) && num_tokens)
			tokens[num_tokens++] = cmdbuf - word_start; // < 0
	}
	// end token list
	tokens[num_tokens] = 0;	
}
