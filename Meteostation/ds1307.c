/*
ds1307 lib 0x01

copyright (c) Davide Gironi, 2013

Released under GPLv3.
Please refer to LICENSE file for licensing information.
*/


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <math.h>

#include "ds1307.h"
#include "SoftI2CMaster.h"

#define SECONDS_PRE_MINUTE		((uint32_t)60)
#define SECONDS_PRE_HOUR		(SECONDS_PRE_MINUTE * 60)
#define SECONDS_PRE_DAY			(SECONDS_PRE_HOUR * 24)

/*
 * days per month
 */
const uint8_t ds1307_daysinmonth [] PROGMEM = { 31,28,31,30,31,30,31,31,30,31,30,31 };
	
static uint8_t ds1307_begin()
{
	uint8_t savedevider = i2c_Half_Delay_value;
	i2c_SetSpeed(I2C_SPEED_MEDIUM);
	i2c_start(DS1307_ADDR | I2C_WRITE);
	return savedevider;
}

static void ds1307_end(uint8_t a)
{
	i2c_stop();
	i2c_Half_Delay_value = a;
}

void ds1307_init() {
	// enable 1s interrupt
	uint8_t savedevider = ds1307_begin();
	i2c_write(0x07);
	i2c_write(1 << 4);
	ds1307_end(savedevider);
}

/*
 * transform decimal value to bcd
 */
uint8_t ds1307_dec2bcd(uint8_t val) {
	return val + 6 * (val / 10);
}

/*
 * transform bcd value to deciaml
 */
static uint8_t ds1307_bcd2dec(uint8_t val) {
	return val - 6 * (val >> 4);
}

/*
 * get number of days since 2000/01/01 (valid for 2001..2099)
 */
static uint16_t ds1307_date2days(uint8_t y, uint8_t m, uint8_t d) {
	uint16_t days = d;
	for (uint8_t i = 1; i < m; ++i)
		days += pgm_read_byte(ds1307_daysinmonth + i - 1);
	if (m > 2 && y % 4 == 0)
		++days;
	return days + 365 * y + (y + 3) / 4 - 1;
}

/*
 * get day of a week
 */
uint8_t ds1307_getdayofweek(uint8_t y, uint8_t m, uint8_t d) {
	uint16_t day = ds1307_date2days(y, m, d);
	return (day + 6) % 7;
}

/*
 * set date
 */
uint8_t ds1307_setdate(struct ds1307DateTime* date) {
	//sanitize data
	/*if (second < 0 || second > 59 ||
		minute < 0 || minute > 59 ||
		hour < 0 || hour > 23 ||
		day < 1 || day > 31 ||
		month < 1 || month > 12 ||
		year < 0 || year > 99)
		return 8;*/

	//sanitize day based on month
	/*if(day > pgm_read_byte(ds1307_daysinmonth + month - 1))
		return 0;*/

	//get day of week
	uint8_t dayofweek = ds1307_getdayofweek(date->year, date->month, date->day);

	//write date
	uint8_t savedevider = ds1307_begin();
	//i2c_start_wait(DS1307_ADDR | I2C_WRITE);
	i2c_write(0x00);//stop oscillator
	
	i2c_write(ds1307_dec2bcd(date->second));
	i2c_write(ds1307_dec2bcd(date->minute));
	i2c_write(ds1307_dec2bcd(date->hour));
	i2c_write(ds1307_dec2bcd(dayofweek));
	i2c_write(ds1307_dec2bcd(date->day));
	i2c_write(ds1307_dec2bcd(date->month));
	i2c_write(ds1307_dec2bcd(date->year));
	//*/
	
	/*
	i2c_write(date->second);
	i2c_write(date->minute);
	i2c_write(date->hour);
	i2c_write(dayofweek);
	i2c_write(date->day);
	i2c_write(date->month);
	i2c_write(date->year);
	//*/
	
	i2c_write(0b10010000); //start oscillator
	ds1307_end(savedevider);

	return 1;
}

/*
 * get date
 */
void ds1307_getdate(struct ds1307DateTime* res) {
	uint8_t savedevider = ds1307_begin();
	i2c_write(0);
	
	uint8_t *f = (uint8_t *)res;
	uint8_t i = 0;
	
	i2c_rep_start(DS1307_ADDR | I2C_READ);
	for (; i < sizeof(struct ds1307DateTime) - 1; ++i)
		f[i] = ds1307_bcd2dec(i2c_readAck());
	f[i] = ds1307_bcd2dec(i2c_readNak());	
	ds1307_end(savedevider);
}

float timeToJD(struct ds1307DateTime* timestamp)
{
	uint8_t a = floor((14 - timestamp->month) / 12.0);
	uint32_t y = timestamp->year + 4800 - a;
	uint32_t m = timestamp->month + 12 * a - 3;
	return timestamp->day + floor((153 * m + 2) / 5.0) + 365 * y + floor(y / 4.0) - floor(y / 100.0) + floor(y / 400.0) - 32045 +
		((float)timestamp->hour - 12) / 24 + (float)timestamp->minute / 1440 + (float)timestamp->second / 86400;
}


// ������ � http://www.onlineconversion.com/julian_date.htm
// ������� function jd_to_cal( jd, form )
void JDtoTimestamp(float JD, struct ds1307DateTime* newtimestamp)
{
	uint32_t	j1, j2, j3, j4, j5;			//scratch
	
	uint32_t	intgr   = floor(JD);
	float		frac	= JD - intgr;
	if (intgr >= 2299161)
	{
		uint32_t tmp = floor(((intgr - 1867216) - 0.25 ) / 36524.25);
		j1 = intgr + 1 + tmp - floor(0.25*tmp);
	}
	else
		j1 = intgr;
	
	//correction for half day offset
	float dayfrac = frac + 0.5;
	if (dayfrac >= 1.0)
	{
		dayfrac -= 1.0;
		++j1;
	}
	
	j2 = j1 + 1524;
	j3 = floor(6680.0 + ( (j2 - 2439870) - 122.1 )/365.25);
	j4 = floor(j3*365.25);
	j5 = floor((j2 - j4)/30.6001);
	
	uint8_t d = floor(j2 - j4 - floor(j5 * 30.6001));
	uint8_t m = floor(j5 - 1);
	if( m > 12 ) 
		m -= 12;
	uint8_t y = floor(j3 - 4715);
	if( m > 2 )   
		--y;
	if( y <= 0 )  
		--y;
	
	//
	// get time of day from day fraction
	//
	uint8_t hr  = floor(dayfrac * 24.0);
	uint8_t mn  = floor((dayfrac * 24.0 - hr) * 60.0);
	float f  = ((dayfrac * 24.0 - hr) * 60.0 - mn) * 60.0;
	uint8_t sc  = floor(f);
	f -= sc;
	if (f > 0.5) 
		++sc;
		
	newtimestamp->year = y;
	newtimestamp->month = m;
	newtimestamp->day = d;
	newtimestamp->hour = hr;
	newtimestamp->minute = mn;
	newtimestamp->second = sc;
}

void timestampAddSeconds(struct ds1307DateTime* timestamp, uint32_t seconds)
{
	float JD = timeToJD(timestamp);
	JDtoTimestamp(JDAddSeconds(JD, seconds), timestamp);
}

float JDAddSeconds(float JD, uint32_t seconds)
{
	return JD + seconds / 86400;
}

void ds1307_readNVRAM(void* dest, uint8_t offset, uint8_t size)
{
	uint8_t savedevider = ds1307_begin();
	i2c_write(NV_RAM_START + offset);
	
	i2c_rep_start(DS1307_ADDR | I2C_READ);
	for (; size > 1; --size, ++dest)
		*((uint8_t*)dest) = i2c_readAck();
	*((uint8_t*)dest) = i2c_readNak();
	
	ds1307_end(savedevider);
}

void ds1307_writeNVRAM(uint8_t offset, void* src, uint8_t size)
{
	uint8_t savedevider = ds1307_begin();
	i2c_write(NV_RAM_START + offset);
	
	for (; size > 0; --size, ++src)
		i2c_write(*((uint8_t*)src));
	
	ds1307_end(savedevider);
}