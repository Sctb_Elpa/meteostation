
#ifndef _SHT_H_
#define _SHT_H_

// ����������� 3,3�
#define SHT_C1			(-4.0)
#define SHT_C2			(0.0405)
#define SHT_C3			(-2.8E-6)

#define SHT_D1			(-39.63)
#define SHT_D2			(0.01)

#define SHT_T1			(0.01)
#define SHT_T2			(0.00008)

typedef struct  
{
	volatile unsigned char* DDR;
	volatile unsigned char* PORT;
	volatile unsigned char* PIN;
	unsigned char pin;
} sPin;

void sht_rd (float *temp, float *truehumid);
//void sht_rd (uint16_t *temp, uint16_t *truehumid);
void comreset (void);

#endif