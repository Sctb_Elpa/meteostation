/*
 * Settings.c
 *
 * Created: 29.05.2014 14:17:36
 *  Author: tolyan
 */ 

#include <avr/eeprom.h>

#include "config/AppConfig.h"
#include "Settings.h"

struct sSettings settings EEMEM =
{
	.TemperatureCoeffs = {
		.F0 = 0,
		.T0 = 0,
		.C = {1, 0, 0}
		},	
	.PressureCoeffs = {
		.Fp0 = 0,
		.Ft0 = 0,
		.A = {0, 0, 0, 1, 0, 0}
		},
	.DisplayPopupVisableTime = 10,
	.Mesure2MesureDelay = 0,
	.FreqMesurePrepareDelay = 1,
	.DisplayOffDelay = DEFAULT_DisplayOffDelay,
	.isDisplayOFFIfUSBPowered = 0,
	.WriteIntervalVariants = {
		.variants = {1, 30, 180},
		.selectd = 0
		},
	.bluetoothVisableTimeout = 60
};

static uint8_t firstrunflag EEMEM = FIRST_RUN;
static uint8_t firstrunflag_RAM = FIRST_RUN;

uint8_t firstRun()
{
#if FIRST_RUN == 1
	if (firstrunflag_RAM)	
		return eeprom_read_byte((void*)&firstrunflag);
	else
		return 0;
#else
	return 0;
#endif
}

void firstRunFinished()
{
	if (firstRun())
	{
		eeprom_update_byte(&firstrunflag, 0);
		firstrunflag_RAM = 0;
	}
}