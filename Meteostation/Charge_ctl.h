/*
 * Charge_ctl.h
 *
 * Created: 16.05.2014 13:10:48
 *  Author: tolyan
 */ 


#ifndef CHRAGE_CTL_H_
#define CHRAGE_CTL_H_

#define BATARY_VOLTAGE_CHANEL		0
#define CHARGE_100					840 //(4.2v)
#define CHARGE_0					640	//(3.2v)

void initCharge_Ctrl();
uint8_t isCharge();
void refreshBataryStatus();
uint8_t ADC_getChargeLevel(uint16_t rawWal);

#endif /* CHRAGE_CTL_H_ */