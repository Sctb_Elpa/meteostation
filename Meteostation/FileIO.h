/*
 * FileIO.h
 *
 * Created: 19.08.2014 16:30:14
 *  Author: tolyan
 */ 


#ifndef FILEIO_H_
#define FILEIO_H_

#include "Lib/FatFs/ff.h"

#define STOP_WRIE_FREE		1

enum saveReason
{
	SAVE_REASON_FORCE,
	SAVE_REASON_TIMER
};

extern const TCHAR saveStrFormat[];

FRESULT mount_extFlash();
FRESULT umount_extFlash();

FRESULT format_extFlash();

FRESULT PrepareStructure();

FRESULT getFreeSpace(uint32_t *sectors, float* pr);

void CheckIsSaveTime();
uint8_t SaveMesureNow(enum saveReason);
uint8_t enableSaveMode();
void disableSaveMode();

void UpdateUsedSpaceInfo();

#endif /* FILEIO_H_ */