/*
 * FastShadulerTimer.c
 *
 * Created: 09.09.2015 13:06:00
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

extern uint8_t tickPassed;

// 16Mhz / 1024 / 223 / 7 = 10 Hz

void InitFastShadulerTimer(char enable)
{
	TCNT2 = 0xff - 223 - 1;
	TCCR2B |= (1 << CS22) | (1 << CS21) |(1 << CS20); //F = clk_io/1024 (16000000/1024)
	TIMSK2 |= 1 << TOIE2; // overflow interrupt
}

static uint8_t c = 0;

ISR(TIMER2_OVF_vect)
{
	TCNT2 = 0xff - 223 - 1;
	if (++c == 7)
	{
		c = 0;
		tickPassed = 1;
	}
}