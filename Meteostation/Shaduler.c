/*
 * Shaduler.c
 *
 * Created: 27.05.2014 14:06:18
 *  Author: tolyan
 */ 

#include <avr/eeprom.h>
#include <string.h>
#include <math.h>
#include <avr/wdt.h>

#include "Display.h"
#include "SSD1306.h"
#include "Charge_ctl.h"
#include "Counters.h"
#include "sht.h"
#include "Config/AppConfig.h"
#include "ButtonsProcessor.h"
#include "FileIO.h"
#include "debug.h"
#include "SSD1306.h"
#include "BT.h"

#include "Shaduler.h"

static enum enCountersEnableVotes MesureEnableVotes = COUNTERS_OFF; // �����������, ��������-�� ��������

static void testpopup()
{
	dataOutputVars.popUp = POPUP_SAVING_MESURE;
}

const struct Task tasks[] EEMEM = 
{
 #if DISPLAY_ENABLED == 1
	{
		RefrashDisplay, 0.5
	}, //�������������� ����� ������ 0,5 ���
 #endif
	{
		ExecMesure, 1
	}, // �������� ��������� ��� � �������
	{
		refreshBataryStatus, 20
	}, // �������� ������ ������ �����
	{
		CheckIsSaveTime, 1
	}, // �������� �������� ������ ��������� ��� � �������
 #if DISPLAY_ENABLED == 1
	{
		processBtns, 0.1
	}, //��������� ������ ��� � 0,1 ���
	{
		UpdateUsedSpaceInfo, 3
	}, // �������� ����� �������� �����
	{
		BT_visableCountdown, 1
	}, // BT visible
 #endif
};

static struct Task tasks_work[sizeof(tasks)/sizeof(struct Task)];

static void reloadTask(uint8_t id)
{
	eeprom_read_block(&tasks_work[id], &tasks[id], sizeof(struct Task));
}

void MesureEnableVoteing(enum enCountersEnableVotes vote, uint8_t voteOn)
{
	if (voteOn)
		MesureEnableVotes |= vote;
	else
		MesureEnableVotes &= ~vote;
};

void Shaduler_enable(uint8_t enable)
{
	if (enable)
	{
		for (uint8_t i = 0; i < sizeof(tasks)/sizeof(struct Task); ++i)
			reloadTask(i);
			
		// ��� ��������� �����
		refreshBataryStatus();
	}
	else
		memset(tasks_work, 0, sizeof(tasks_work));
}


void Tick()
{
#if USE_WDT == 1
	wdt_reset();
#endif

#if FAST_SHADULER
	const float tick = 0.1;
#else
	const float tick = DOUBLE_REFRASH_RATE ? 0.5 : 1;
#endif
	for (uint8_t TaskID = 0; TaskID < sizeof(tasks)/sizeof(struct Task); ++TaskID)
	{
		taskRoutine Routine = tasks_work[TaskID].Routine;
		if (Routine)
			if ((tasks_work[TaskID].period -= tick) <= 0)
			{
				Routine();
#if USE_WDT == 1
				wdt_reset();
#endif
				reloadTask(TaskID);
			}
	}
}

void ExecMesure()
{
#if MESURMENT_ENABLE == 1
	// ��������� ��������, ���� �����-�� �� ������� ��� �����
	if (MesureEnableVotes != COUNTERS_OFF)
	{
		EnableCounters(1); 
		float dummy;
		// ���������
		sht_rd(&dummy, &dataOutputVars.Humidity); // �����������, ���������
		calcFsensOutputVals(&dataOutputVars.Temperature, &dataOutputVars.Pressure); // �����������, ��������
			
		//�����
		ds1307_getdate(&dataOutputVars.dateTime);
	}
	else
		EnableCounters(0);
#else
	ds1307_getdate(&dataOutputVars.dateTime);
#endif
}