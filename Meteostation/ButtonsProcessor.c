/*
 * ButtonsProcessor.c
 *
 * Created: 30.05.2014 14:26:00
 *  Author: tolyan
 */ 

#include <stddef.h>
#include <avr/interrupt.h>
#include <stdio.h>

#include <LUFA/Drivers/Board/LEDs.h>

#include "Display.h"
#include "debug.h"

#include "ButtonsProcessor.h"

static void unblock_longPress(enum enBtnID b);

struct sButtonCallbacks btns[2];

static uint8_t getBtnVal(enum enBtnID btn)
{
	switch(btn)
	{
		case BTN_OK:
			return (~BTN_OK_IN) & (1 << BTN_OK_PIN);
		case BTN_SELECT:
			return (~BTN_SEL_IN) & (1 << BTN_SEL_PIN);
		default:
			return 0;
	}	
}

void ProcessButtonIvent(enum enBtnID b)
{
	uint8_t ls = btns[b].lastState;
	switch (ls & 0b11)
	{
		case 0b11 :
			//release
			btns[b].lastState = (ls << 1);
			if (btns[b].onRelease)
			{
				btns[b].onRelease();
			}
			if (!btns[b].LongPressDetected && btns[b].onClick)
			{
				btns[b].onClick();
			}
			break;
		case 0b00 :
			// press
			btns[b].LongPressDetected = 0;
			btns[b].lastState = (ls << 1) | 1;
			if (btns[b].onPress)
			{
				btns[b].onPress();
			}
		break;
	}
}

void ButtonsInit()
{
	// input
	BTN_OK_DIR &= ~(1 << BTN_OK_PIN);
	BTN_SEL_DIR &= ~(1 << BTN_SEL_PIN);

	//pull up
	BTN_OK_PORT |= 1 << BTN_OK_PIN;
	BTN_SEL_PORT |= 1 << BTN_SEL_PIN;
}

void processBtns()
{
	enum enBtnID keyEvent = BTN_DUMMY;
	static uint8_t prevVal = 0;
	
	uint8_t curentVal = ((~BTN_OK_IN) & (1 << BTN_OK_PIN)) | ((~BTN_SEL_IN) & (1 << BTN_SEL_PIN));
	
	uint8_t diff = curentVal ^ prevVal;
	
	prevVal = curentVal;
	
	if (diff)
	{
		if (diff & (1 << BTN_OK_PIN))
			keyEvent = BTN_OK;
		else
			keyEvent = BTN_SELECT;
	}
	
	if (keyEvent != BTN_DUMMY)
		ProcessButtonIvent(keyEvent);
	
	for (uint8_t i = 0; i < sizeof(btns) / sizeof(struct sButtonCallbacks) ; ++i)
	{
		uint8_t* p = &btns[i].lastState;
		*p = (*p << 1) | (getBtnVal(i) ? 1 : 0);
		if ((*p & 0b1111111) == 0b1111111)
		{
			*p &= ~1;
			btns[i].LongPressDetected = 1;
			if (btns[i].onLongPush)
				btns[i].onLongPush();
		}
	}
}