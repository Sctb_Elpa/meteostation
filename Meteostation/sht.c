#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>

#include "delay_wraper.h"
#include "sht.h"

#define SHT_DATA_PORT			PORTC
#define SHT_DATA_PIN			PINC
#define SHT_DATA_DDR			DDRC
#define SHT_DATA_PIN_N			7

#define SHT_CLK_PORT			PORTC
#define SHT_CLK_PIN				PINC
#define SHT_CLK_DDR				DDRC
#define SHT_CLK_PIN_N			6

enum enPin
{
	sht_data_pin, 
	sht_clk_pin
};

static void output_float(enum enPin p)
{
	if (p == sht_data_pin)
		SHT_DATA_DDR &= ~(1 << SHT_DATA_PIN_N);
	else
		SHT_CLK_DDR &= ~(1 << SHT_CLK_PIN_N);
}

static void output_bit(enum enPin p, char value)
{
	if (p == sht_data_pin)
	{
		// pulled up
		if (value)	
			output_float(p);		
		else
		{
			SHT_DATA_DDR |= 1 << SHT_DATA_PIN_N;
			SHT_DATA_PORT &= ~(1 << SHT_DATA_PIN_N);
		}
	}
	else
	{
		// raw
		SHT_CLK_DDR |= 1 << SHT_CLK_PIN_N;
		if (value)
			SHT_CLK_PORT |= 1 << SHT_CLK_PIN_N;
		else
			SHT_CLK_PORT &= ~(1 << SHT_CLK_PIN_N);
	}
}

static char input(enum enPin p)
{
	if (p == sht_data_pin)
		return (SHT_DATA_PIN & (1 << SHT_DATA_PIN_N)) ? 1 : 0;
	else
		return (SHT_CLK_PIN & (1 << SHT_CLK_PIN_N)) ? 1 : 0;
}


//***** SHT75 Ba�latma *****

void comstart (void)
{
 output_float(sht_data_pin);  //data high
 output_bit(sht_clk_pin, 0);  //clk low
 delay_us(1);
 output_bit(sht_clk_pin, 1);  //clk high
 delay_us(1);
 output_bit(sht_data_pin, 0); //data low
 delay_us(1);
 output_bit(sht_clk_pin, 0);  //clk low
 delay_us(2);
 output_bit(sht_clk_pin, 1);  //clk high
 delay_us(1);
 output_float(sht_data_pin);  //data high
 delay_us(1);
 output_bit(sht_clk_pin, 0);  //clk low
}


//***** SHT75 yazma fonksiyonu*****

bool comwrite (uint8_t iobyte)
{
 uint8_t i, mask = 0x80;
 bool ack;

 //Komut g�nderilir
 delay_us(4);
 for(i=0; i<8; i++)
  {
   output_bit(sht_clk_pin, 0);                          //clk low
   if((iobyte & mask) > 0) 
		output_float(sht_data_pin);  //data high if MSB high
   else 
		output_bit(sht_data_pin, 0);                    //data low if MSB low
   delay_us(1);
   output_bit(sht_clk_pin, 1);                          //clk high
   delay_us(1);
   mask = mask >> 1;                                    //shift to next bit
  }

 //Shift in ack
 output_bit(sht_clk_pin, 0);  //clk low
 delay_us(1);
 ack = input(sht_data_pin);   //get ack bit
 output_bit(sht_clk_pin, 1);  //clk high
 delay_us(1);
 output_bit(sht_clk_pin, 0);  //clk low
 return(ack);
}


//***** Function to read data from SHT75 *****

uint16_t comread (void)
{
 uint8_t i;
 uint16_t iobyte = 0;
 const uint16_t mask0 = 0x0000;
 const uint16_t mask1 = 0x0001;

 //shift in MSB data
 for(i=0; i<8; i++)
  {
   iobyte = iobyte << 1;
   output_bit(sht_clk_pin, 1);                //clk high
   delay_us(1);
   if (input(sht_data_pin)) 
		iobyte |= mask1;					  //shift in data bit
   else 
		iobyte |= mask0;
   output_bit(sht_clk_pin, 0);                //clk low
   delay_us(1);
  }

 //send ack 0 bit
 output_bit(sht_data_pin, 0); //data low
 delay_us(1);
 output_bit(sht_clk_pin, 1);  //clk high
 delay_us(2);
 output_bit(sht_clk_pin, 0);  //clk low
 delay_us(1);
 output_float(sht_data_pin);  //data high

 //shift in LSB data
 for(i=0; i<8; i++)
  {
   iobyte = iobyte << 1;
   output_bit(sht_clk_pin, 1);                //clk high
   delay_us(1);
   if (input(sht_data_pin)) 
	iobyte |= mask1;  //shift in data bit
   else 
	iobyte |= mask0;
   output_bit(sht_clk_pin, 0);                //clk low
   delay_us(1);
  }

 //send ack 1 bit
 output_float(sht_data_pin);  //data high
 delay_us(1);
 output_bit(sht_clk_pin, 1);  //clk high
 delay_us(2);
 output_bit(sht_clk_pin, 0);  //clk low

 return(iobyte);
}


//***** Function to wait for SHT75 reading *****

void comwait (void)
{
 uint16_t sht_delay;

 output_float(sht_data_pin);                     //data high
 output_bit(sht_clk_pin, 0);                     //clk low
  delay_us(1);
 for(sht_delay=0; sht_delay<30000; sht_delay++)  // wait for max 300ms
  {
   if (!input(sht_data_pin)) 
		break;              //if sht_data_pin low, SHT75 ready
   delay_us(10);
  }
}


//***** Function to reset SHT75 communication *****

void comreset (void)
{
 uint8_t i;

 output_float(sht_data_pin);    //data high
 output_bit(sht_clk_pin, 0);    //clk low
 delay_us(2);
 for(i=0; i<9; i++)
  {
   output_bit(sht_clk_pin, 1);  //toggle clk 9 times
   delay_us(2);
   output_bit(sht_clk_pin, 0);
   delay_us(2);
 }
 comstart();
}


//***** Function to soft reset SHT75 *****

void sht_soft_reset (void)
{
 comreset();           //SHT75 communication reset
 comwrite(0x1e);       //send SHT75 reset command
 delay_ms(15);         //pause 15 ms
}


//***** Function to measure SHT75 temperature *****

uint16_t measuretemp (void)
{
 bool ack;
 uint16_t iobyte;

 comstart();             //alert SHT75
 ack = comwrite(0x03);   //send measure temp command and read ack status
 if(ack == 1) return 0;
 comwait();              //wait for SHT75 measurement to complete
 iobyte = comread();     //read SHT75 temp data
 return(iobyte);
}


//***** Function to measure SHT75 RH *****

uint16_t measurehumid (void)
{
 bool ack;
 uint16_t iobyte;

 comstart();            //alert SHT75
 ack = comwrite(0x05);  //send measure RH command and read ack status
 if(ack == 1) 
	return 0;
 comwait();             //wait for SHT75 measurement to complete
 iobyte = comread();    //read SHT75 temp data
 return(iobyte);
}


//***** Function to calculate SHT75 temp & RH *****

void calculate_data (uint16_t temp, uint16_t humid, float  *tc, float *rhlin, float *rhtrue)
{
 float rh;

 //calculate temperature reading
 *tc = (float) temp * SHT_D2  + SHT_D1;

 //calculate Real RH reading
 rh = (float) humid;

 *rhlin = rh * (SHT_C2 + rh * SHT_C3) + SHT_C1;

 //calculate True RH reading
 *rhtrue = (*tc - 25.0) * (SHT_T1 + SHT_T2 * rh) + *rhlin;
 }


//***** Function to measure & calculate SHT75 temp & RH *****

//void sht_rd (uint16_t *temp, uint16_t *truehumid)
void sht_rd (float *temp, float *truehumid)
{
 uint16_t restemp, reshumid;
 float realhumid;

 restemp = measuretemp();    //measure temp
 reshumid = measurehumid();  //measure RH
calculate_data (restemp, reshumid, temp, &realhumid, truehumid);  //calculate temp & RH
//*temp= restemp;
//*truehumid = reshumid;
}


//***** Function to initialise SHT75 on power-up *****
/*
void sht_init (void)
{
 comreset();    //reset SHT75
 delay_ms(20);  //delay for power-up
}
void main()
{
 float restemp, truehumid;
 lcd_init();
sht_init();

 while(1)
 {
  sht_rd (restemp, truehumid);
  printf(lcd_putc,"Temp : %3.1f %cC   ", restemp, 223);
  printf(lcd_putc,"\nRH   : %3.1f %%   ", truehumid);
  delay_ms(500);        //delay 500 ms between reading to prevent self heating of sensor
 }
}

*/