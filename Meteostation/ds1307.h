/*
ds1307 lib 0x01

copyright (c) Davide Gironi, 2013

Released under GPLv3.
Please refer to LICENSE file for licensing information.

References: parts of the code taken from https://github.com/adafruit/RTClib
*/


#ifndef DS1307_H
#define DS1307_H

#ifdef __cplusplus
extern "C" {
	#endif
	
#include <stdint.h>

#define NV_RAM_START			0x08

struct ds1307DateTime 
{
	uint8_t second;
	uint8_t minute; 
	uint8_t hour;
	uint8_t DoW;
	uint8_t day;
	uint8_t month;
	uint8_t year;
};

//definitions
#define DS1307_ADDR (0x68<<1) //device address

/** defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start() */
#define I2C_READ    1

/** defines the data direction (writing to I2C device) in i2c_start(),i2c_rep_start() */
#define I2C_WRITE   0

//path to i2c fleury lib
//#define DS1307_I2CFLEURYPATH "../i2chw/i2cmaster.h" //define the path to i2c fleury lib
//#define DS1307_I2CINIT 1 //init i2c

//functions
void ds1307_init();
uint8_t ds1307_setdate(struct ds1307DateTime* date);
void ds1307_getdate(struct ds1307DateTime* res);

// datetime operations
void timestampAddSeconds(struct ds1307DateTime* timestamp, uint32_t seconds);
float JDAddSeconds(float JD, uint32_t seconds);
float timeToJD(struct ds1307DateTime* timestamp);
void JDtoTimestamp(float JD, struct ds1307DateTime* newtimestamp);

// NV RAM
void ds1307_readNVRAM(void* dest, uint8_t offset, uint8_t size);
void ds1307_writeNVRAM(uint8_t offset, void* src, uint8_t size);

#ifdef __cplusplus
}
#endif

#endif

