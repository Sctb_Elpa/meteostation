/*
 * Settings.h
 *
 * Created: 29.05.2014 14:18:19
 *  Author: tolyan
 */ 


#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <avr/eeprom.h>

#include "Counters.h"
#include "debug.h"

#define DEFAULT_DisplayOffDelay			15


#define NVRAM_DISPLAYSTATUS_OFFSET		(NVRAM_DEBUG_OFFSET + sizeof(struct stackInfo))


#define SAVE_NVRAM(addr, val)			ds1307_writeNVRAM(addr, &val, 1);
#define READ_NVRAM(addr, pval)			ds1307_readNVRAM(&pval, addr, 1);

struct asist1
{
	uint16_t	v[3];
};

struct sWriteIntervalVariants 
{
	uint16_t	  variants[3];
	uint8_t		  selectd;
};

struct sSettings
{
	// ������������ ��� ������� �����������
	struct sTemperatureCoeffs		TemperatureCoeffs;
	// ������������ ��� ������� ��������
	struct sPressureCoeffs			PressureCoeffs;
	
	// ����� ������ ������
	uint8_t							DisplayPopupVisableTime;
	
	// �������� ���������� ��������� �������� (�� ��������� � ������ ������)
	// * 0,5 ���
	uint8_t							Mesure2MesureDelay;
	
	// ����� ���������� ����������� ��������� ��������� �������� [c��]
	uint8_t							FreqMesurePrepareDelay;
	
	// �������� ������� ������� [c��] (< 0 - �� ���������)
	int8_t							DisplayOffDelay;
	
	// ��������� ������� ��� ������� �� USB ?
	uint8_t							isDisplayOFFIfUSBPowered;
	
	// �������� ��������� ������ ��������� (����� ������������� ����)
	struct sWriteIntervalVariants	WriteIntervalVariants;	
	
	// ������� "���������" bluetooth, ����� bluetooth ����� ��������
	// ������, ���� � ������� ������������ ������� ������ ������� ����� �����
	uint8_t							bluetoothVisableTimeout;
};

uint8_t firstRun();
void firstRunFinished();				

extern struct sSettings settings;

#endif /* SETTINGS_H_ */