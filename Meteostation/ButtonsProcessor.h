/*
 * ButtonsProcessor.h
 *
 * Created: 30.05.2014 14:26:35
 *  Author: tolyan
 */ 


#ifndef BUTTONS_PROCESSOR_H_
#define BUTTONS_PROCESSOR_H_

#include <stdint.h>
/*
#define BTN_OK_PORT		PORTD
#define BTN_OK_DIR		DDRD
#define BTN_OK_IN		PIND
#define BTN_OK_PIN		1 
*/
#define BTN_OK_PORT		PORTE
#define BTN_OK_DIR		DDRE
#define BTN_OK_IN		PINE
#define BTN_OK_PIN		4

#define BTN_SEL_PORT	PORTE
#define BTN_SEL_DIR		DDRE
#define BTN_SEL_IN		PINE
#define BTN_SEL_PIN		5

enum enBtnID
{
	BTN_OK = 0,
	BTN_SELECT = 1,	
	BTN_DUMMY = 0xff
};

typedef void (*BtnCallBack)(void);

struct sButtonCallbacks
{
	uint8_t lastState;
	uint8_t LongPressDetected;
	BtnCallBack onRelease;
	BtnCallBack	onPress;
	BtnCallBack onLongPush;
	BtnCallBack onClick;
};

void ButtonsInit();
void processBtns(void);

extern struct sButtonCallbacks btns[2];

#endif /* BUTTONS_PROCESSOR_H_ */