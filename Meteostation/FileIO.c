/*
 * FileIO.c
 *
 * Created: 19.08.2014 16:31:53
 *  Author: tolyan
 */ 

#include <stddef.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "Display.h"
#include "debug.h"
#include "Settings.h"
#include "Charge_ctl.h"
#include "SSD1306.h"

#include "FileIO.h"

#define TIMESTAMP		"%02i:%02i:%02i"
#define DOT_ZAP			";"
#define _NEWLINE		"\n"

static const TCHAR diskname[] PROGMEM = "0:";
static const TCHAR series_dir_name[] PROGMEM = "/series";
static const TCHAR oneshot_dir_name[] PROGMEM = "/oneshot";

static const TCHAR header[] PROGMEM = "Time;Temperature [*C];Pressure [mmHg];Humidity [%];Charge [%]\n";
const TCHAR saveStrFormat[] PROGMEM = TIMESTAMP\
											DOT_ZAP\
											"%.2f"\
											DOT_ZAP\
											"%.2f"\
											DOT_ZAP\
											"%.2f"\
											DOT_ZAP\
											"%i"\
											_NEWLINE;
											
static FIL seriesFile;

/** FAT Fs structure to hold the internal state of the FAT driver for the Dataflash contents. */
static FATFS DiskFATState;

// ������ �� ��������� ������
// 0 - ������� �������
// > 0 - �����
static uint16_t ToSaveCounter = 0;

FRESULT mount_extFlash()
{
	/* Mount the storage device */
	return f_mount(&DiskFATState,	// ���������
				 diskname,				// ������
				 1					// 1 - ������������ ������ � ��������� ����������
				 );
}

FRESULT umount_extFlash()
{
	/* Mount the storage device */
	return f_mount(NULL,				// ���������
					diskname,			// ������
					1
					);
	
}

FRESULT format_extFlash()
{
	FRESULT res = f_mkfs(diskname, // ����
						  1, // FDISK(0) - ������� or SFD(1) - ��� ��������
						  0); // cluster size - default
	if (res == FR_OK)
	{
		res = mount_extFlash();
		if (res == FR_OK)
			res = PrepareStructure();
	}
	return res;	
}

FRESULT PrepareStructure()
{
	FRESULT res;
	TCHAR buff[sizeof(series_dir_name) > sizeof(oneshot_dir_name) ? sizeof(series_dir_name) : sizeof(oneshot_dir_name)];
	
	strcpy_P(buff, series_dir_name);
	res = f_mkdir(buff);
	if ((res != FR_OK) && (res != FR_EXIST))
		return res;
		
	strcpy_P(buff, oneshot_dir_name);
	res = f_mkdir(buff);
	if (res == FR_EXIST)
		res = FR_OK;
	return res;
}

FRESULT getFreeSpace(uint32_t *sectors, float* pr)
{
	DWORD fre_clust, fre_sect, tot_sect;

	FATFS *fs;
	/* Get volume information and free clusters of drive 1 */
	FRESULT res = f_getfree(diskname, &fre_clust, &fs);
	if (res)
		return res;

	/* Get total sectors and free sectors */
	tot_sect = (fs->n_fatent - 2) * fs->csize;
	fre_sect = fre_clust * fs->csize;
	
	if (sectors)
		*sectors = (uint32_t)fre_sect - 1;
	if (pr)
		*pr = ((float)(fre_sect - 1)) / (float)tot_sect * 100.0;
	return res;
}

static uint8_t IsSpaceAvalable()
{
	uint32_t freesp;
	getFreeSpace(&freesp, NULL);
	return !(freesp <= STOP_WRIE_FREE);
}

uint8_t SaveMesureNow(enum saveReason reason)
{
	char buff[80] = {'/'};
	FRESULT res;
	UINT toWrite, writen;
	switch (reason)
	{
	case SAVE_REASON_FORCE:
		{
			if (!IsSpaceAvalable())
			{
				dataOutputVars.popUp = POPUP_MEMORY_FULL;
				return 0;
			}
			FIL f;
			strcpy_P(buff + 1, oneshot_dir_name);
			sprintf_P(buff + strlen(buff), PSTR("/%02i%02i%4i.csv"),
						(int)dataOutputVars.dateTime.day,
						(int)dataOutputVars.dateTime.month,
						(int)dataOutputVars.dateTime.year + 2000);
									
			int exists = 0;
			res = f_open(&f, buff, FA_WRITE | FA_OPEN_ALWAYS);
						
			switch (res)
			{
				case FR_NO_FILESYSTEM:
					res = format_extFlash();
					if (res != FR_OK)
						break;
					res = f_open(&f, buff, FA_WRITE | FA_CREATE_NEW);
					break;
				case FR_NO_FILE:
				case FR_NO_PATH:
					PrepareStructure();
					res = f_open(&f, buff, FA_WRITE | FA_CREATE_NEW);
					break;
				case FR_NOT_ENABLED:
					// ��� ����� ����� ������� ����
					dataOutputVars.popUp = POPUP_MEMORY_FULL;
					return 0;
				default:
					break;
			}
			if (res)
			{
				DEBUG_MSG("OFOF: res=%i", res);
				return 0;
			}
			else
				if (f.fsize != 0)
				{
					exists = 1;
					res = f_lseek(&f, f.fsize);
					if (res)
					{
						DEBUG_MSG("SF: %i (%i)", f.fsize, res);
						return 0;
					}
					
				}
			
			if (!exists)
			{
				strcpy_P(buff, header);
				toWrite = strlen(buff);
				res = f_write(&f, buff, toWrite, &writen);
				if (
					res	||		// error
					(toWrite != writen) //disk full
					)
				{
					dataOutputVars.popUp = POPUP_MEMORY_FULL;
					f_close(&f);
					return 0; 
				}
			}
			sprintf_P(buff, saveStrFormat,
						(int)dataOutputVars.dateTime.hour,
						(int)dataOutputVars.dateTime.minute,
						(int)dataOutputVars.dateTime.second,
						dataOutputVars.Temperature,
						dataOutputVars.Pressure,
						dataOutputVars.Humidity,
						(int)ADC_getChargeLevel(dataOutputVars.ADC_RAWVal)
						);
			toWrite = strlen(buff);
			res = f_write(&f, buff, toWrite, &writen);
			if (
				res	||		// error
				(toWrite != writen) //disk full
				)
			{
				dataOutputVars.popUp = POPUP_MEMORY_FULL;
				f_close(&f);
				return 0;
			}
			f_close(&f);
		}
		break;
		
	case SAVE_REASON_TIMER:
		{
			if (!IsSpaceAvalable())
			{
				dataOutputVars.popUp = POPUP_MEMORY_FULL;
				disableSaveMode();
				return 0;	
			}
			sprintf_P(buff, saveStrFormat,
						(int)dataOutputVars.dateTime.hour,
						(int)dataOutputVars.dateTime.minute,
						(int)dataOutputVars.dateTime.second,
						dataOutputVars.Temperature,
						dataOutputVars.Pressure,
						dataOutputVars.Humidity,
						(int)ADC_getChargeLevel(dataOutputVars.ADC_RAWVal)
						);
			toWrite = strlen(buff);
			res = f_write(&seriesFile, buff, toWrite, &writen);
			if (
				res	||		// error
				(toWrite != writen) //disk full
				)
			{
				dataOutputVars.popUp = POPUP_MEMORY_FULL;
				disableSaveMode();
				return 0;
			}
			f_sync(&seriesFile);
		}
		break;
	}
	
	UpdateUsedSpaceInfo();
	
	return 1;
}

void disableSaveMode()
{
	dataOutputVars.headerStatus &= ~H_RECORDING;
	f_close(&seriesFile);
}

static resetSaveTimer()
{
	uint8_t selectedIntervel;
	eeprom_read_block(&selectedIntervel, &settings.WriteIntervalVariants.selectd, sizeof(uint8_t));
	eeprom_read_block(&ToSaveCounter, &settings.WriteIntervalVariants.variants[selectedIntervel], sizeof(uint16_t));
	--ToSaveCounter; // ����� �������� ����������.
}

uint8_t enableSaveMode()
{
	if (!IsSpaceAvalable())
		return 0;
	
	char buff[sizeof(header) + 1];

	strcpy_P(buff, series_dir_name);
	sprintf_P(buff + strlen(buff), PSTR("/%02i%02i%4i"),
				(int)dataOutputVars.dateTime.day,
				(int)dataOutputVars.dateTime.month,
				(int)dataOutputVars.dateTime.year + 2000);
	
	FRESULT res = f_mkdir(buff);
	switch (res)
	{
		case FR_NO_FILESYSTEM:
			res = format_extFlash();
				if (res != FR_OK)
					break;
			res = f_mkdir(buff);
			break;
		case FR_NO_FILE:
		case FR_NO_PATH:
			PrepareStructure();
			res = f_mkdir(buff);
			break;
		default:
			break;
	}
	sprintf_P(buff + strlen(buff), PSTR("/%02i%02i%02i.csv"),
				(int)dataOutputVars.dateTime.hour,
				(int)dataOutputVars.dateTime.minute,
				(int)dataOutputVars.dateTime.second);
	
	res = f_open(&seriesFile, buff, FA_WRITE | FA_CREATE_ALWAYS);
	switch (res)
	{
		case FR_OK:
			break;
		case FR_NOT_ENABLED:
			// ��� ����� ����� ������� ����
			return 0;	
		case FR_INVALID_NAME:
			DEBUG_MSG("SFOF(6): %s", buff);	
			return 0;
		default:
			DEBUG_MSG("SFOF: res=%i", res);
			return 0;
	}
	
	UINT toWrite, writen;
	
	// �����
	strcpy_P(buff, header);
	toWrite = strlen(buff);
	res = f_write(&seriesFile, buff, toWrite, &writen);
	if (
		res	||		// error
		(toWrite != writen) //disk full
		)
	{
		f_close(&seriesFile);
		return 0;
	}
	// ����� ��������
		
	// ������ ���������
	dataOutputVars.headerStatus |= H_RECORDING;
	
	ToSaveCounter = 0;
	
	return 1;
}

void CheckIsSaveTime()
{
	if (dataOutputVars.headerStatus & H_RECORDING) // ������ ���������
	{
		if (!ToSaveCounter--)
		{
			resetSaveTimer(); // restart counter
			SaveMesureNow(SAVE_REASON_TIMER);
		}
		
		// �� ���� �� �������� �������� ?
		MesureEnableVoteing(SAVE_VOTE, ToSaveCounter < eeprom_read_byte(&settings.FreqMesurePrepareDelay) ? 1 : 0);
	}
	else
		MesureEnableVoteing(SAVE_VOTE, 0);
}

void UpdateUsedSpaceInfo()
{
	if(isDisplayOn())
	{
		float free_pr;
		FRESULT res = getFreeSpace(NULL, &free_pr);
		if (res == FR_OK)
			updateUsedSpaceIndicator(100.0 - free_pr);
		else
			updateUsedSpaceIndicator(NAN);
	}
}