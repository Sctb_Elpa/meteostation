/*
 * UI.h
 *
 * Created: 02.06.2014 13:30:27
 *  Author: tolyan
 */ 


#ifndef UI_H_
#define UI_H_

enum enUIState
{
	UI_SLEEP,
	UI_DEFAULT,
	UI_SELECT_SAVE_INTERVAL,
	UI_CLEAR_MEMORY
};

void UpdateCallbacks(enum enUIState UIState);

#endif /* UI_H_ */