/*
 * INT0.c
 *
 * Created: 15.05.2014 10:51:23
 *  Author: tolyan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#include "Counters.h"
#include "Shaduler.h"

#include "INT0.h"

#define INT0_PIN	0
#define INT0_DDR	DDRD

uint8_t tickPassed = 0;

void INT0_Enable(char enable)
{
	INT0_DDR &= ~(1 << INT0_PIN); //input
	if (enable)
	{
#if DOUBLE_REFRASH_RATE == 1
		EICRA |= 1 << ISC00;
		EICRA &= ~(1 << ISC01);
#else
		//falling age
		EICRA |= 1 << ISC01;
		EICRA &= ~(1 << ISC00);
#endif	
		EIMSK |= 1<< INT0;
	}
	else
		EIMSK &= ~(1<<INT0);
}

// interrupt itself
ISR(INT0_vect)
{
#if FAST_SHADULER == 0
	tickPassed = 1;
#endif

#if USE_WDT == 1
	wdt_reset();
#endif
}