/*
 * filter.h
 *
 * Created: 22.09.2014 8:41:22
 *  Author: tolyan
 */ 


#ifndef FILTER_H_
#define FILTER_H_

struct sFilter
{
	uint8_t rang;
	float* const coeffs; //pointer to pgmspace
	float* values;
	uint8_t valuesLoaded;	
};

float filter(struct sFilter* f, float input);
void filterReset(struct sFilter* f);

#endif /* FILTER_H_ */