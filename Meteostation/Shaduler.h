/*
 * Shaduler.h
 *
 * Created: 27.05.2014 14:08:22
 *  Author: tolyan
 */ 


#ifndef UPDATE_MANAGER_H_
#define UPDATE_MANAGER_H_

#define DOUBLE_REFRASH_RATE		1

typedef void (*taskRoutine)(void);

struct Task
{
	taskRoutine Routine;
	float period;	
};

enum enCountersEnableVotes
{
	COUNTERS_OFF		= 0,
	DiSPLAY_VOTE		= 1 << 0,
	USB_VOTE			= 1 << 1,
	SAVE_VOTE			= 1 << 2,
	BT_VOTE				= 1 << 3	
};

// �������� ������
void MesureEnableVoteing(enum enCountersEnableVotes vote, uint8_t voteOn);

void Shaduler_enable(uint8_t enable);
void Tick();
void ExecMesure();

#endif /* INCFILE1_H_ */