/*
 * BT.h
 *
 * Created: 28.08.2014 15:42:37
 *  Author: tolyan
 */ 

#ifndef _BT_H_
#define _BT_H_

#include <avr/io.h>

#include "Lib/uart.h"

#define BT_RESET_PORT				PORTC
#define BT_RESET_DDR				DDRC
#define BT_RESET_PIN_N				4

#define STR_BUFF_LEN				128

#define BT_BOUD_RATE				(38400UL)

#define BT_AUTODISABLE_TIMEOUT		300 // 5 min

enum enStrStatus
{
	STR_OK,
	STR_NOT_READY	
};

enum enMode
{
	MODE_INIT = 1,
	MODE_COMMAND = 2,
	MODE_DATA = 3
};

void BT_Init();
void BT_SetVisable(uint8_t visable);
void ProcessBT();
void BT_visableCountdown();

extern const char newLine[];

#endif /* _BT_H_ */ 