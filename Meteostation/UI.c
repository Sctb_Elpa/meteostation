/*
 * UI.c
 *
 * Created: 02.06.2014 13:29:59
 *  Author: tolyan
 */ 

#include <string.h>
#include <avr/eeprom.h>

#include "Settings.h"
#include "Display.h"
#include "ButtonsProcessor.h"
#include "UI.h"
#include "FileIO.h"
#include "UsbConnectISR.h"
#include "BT.h"
#include "debug.h"

static void ret2DefaultCallback()
{
	UpdateCallbacks(UI_DEFAULT);
	forceDisplayRefrash = 1;	
}

static void forceSaveCurentResult()
{
	if (isUSBConnected())
		return;
	dataOutputVars.popUp = POPUP_SAVING_MESURE;
	SaveMesureNow(SAVE_REASON_FORCE);
	forceDisplayRefrash = 1;
}

static void enableBTCallback()
{
	BT_SetVisable(1);
	forceDisplayRefrash = 1;
}

static void saveIntervalSelector_show()
{
	if (isUSBConnected())
		return;
	UpdateCallbacks(UI_SELECT_SAVE_INTERVAL);
	dataOutputVars.popUp = POPUP_SELECT_SAVE_INTERVAL;
	forceDisplayRefrash = 1;
}

static void saveIntervalSelector_change()
{
	uint8_t n = eeprom_read_byte(&settings.WriteIntervalVariants.selectd);
	n = (n + 1) % (sizeof(struct asist1) / sizeof(uint16_t));
	eeprom_write_byte(&settings.WriteIntervalVariants.selectd, n);
	
	saveIntervalSelector_show();
}

static void confurmWriteInterval()
{
	if (enableSaveMode())
		dataOutputVars.popUp = POPUP_SAVING_STARTED;
	else
		dataOutputVars.popUp = POPUP_MEMORY_FULL;
	UpdateCallbacks(UI_DEFAULT);	
	forceDisplayRefrash = 1;
}

static void cancelWriting()
{
	UpdateCallbacks(UI_DEFAULT);
	dataOutputVars.popUp = POPUP_SAVING_STOPPED;
	disableSaveMode();
	forceDisplayRefrash = 1;
}

static void askClearMemory()
{
	UpdateCallbacks(UI_CLEAR_MEMORY);
	dataOutputVars.popUp = POPUP_ASK_CLEAR_MEMORY;
	forceDisplayRefrash = 1;
}

static void confirmClearMemory()
{
	format_extFlash();
	UpdateCallbacks(UI_DEFAULT);
	dataOutputVars.popUp = POPUP_MEMORY_CLEARED;
	forceDisplayRefrash = 1;
}

static void cancelClearMemory()
{
	UpdateCallbacks(UI_DEFAULT);
	dataOutputVars.popUp = POPUP_NONE;
	prepareScreen();
	forceDisplayRefrash = 1;
}

static void clearCallbacks()
{
	for(uint8_t i = 0; i < sizeof(btns) / sizeof(struct sButtonCallbacks); ++i)
		memset(&btns[i].onPress, 0, sizeof(BtnCallBack) * 3);
}

void UpdateCallbacks(enum enUIState UIState)
{
	clearCallbacks();
	switch(UIState)
	{
		case UI_SLEEP:
			btns[BTN_OK].onClick = ret2DefaultCallback;
			btns[BTN_SELECT].onClick = ret2DefaultCallback;
			break;
		case UI_DEFAULT:
			btns[BTN_OK].onClick = 
				dataOutputVars.headerStatus & H_RECORDING ? 
					ret2DefaultCallback : forceSaveCurentResult;
			btns[BTN_OK].onLongPush = 
				dataOutputVars.headerStatus & H_RECORDING ?
					cancelWriting : saveIntervalSelector_show;
			btns[BTN_SELECT].onClick = ret2DefaultCallback;
			btns[BTN_SELECT].onLongPush = enableBTCallback;
			break;
		case UI_SELECT_SAVE_INTERVAL:
			btns[BTN_OK].onClick = confurmWriteInterval;
			btns[BTN_SELECT].onClick = saveIntervalSelector_change;
			btns[BTN_SELECT].onLongPush = askClearMemory;
			break;
		case UI_CLEAR_MEMORY:
			btns[BTN_OK].onClick = confirmClearMemory;
			btns[BTN_SELECT].onClick = cancelClearMemory;
			break;
		default:
			UpdateCallbacks(UI_DEFAULT);
			break;
	}
}