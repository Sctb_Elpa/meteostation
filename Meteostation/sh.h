/*
 * sh.h
 *
 * Created: 03.09.2014 10:39:24
 *  Author: tolyan
 */ 


#ifndef SH_H_
#define SH_H_

#define MAX_NUM_TOKENS		10

enum enTokes
{
	TOK_NULL = 0,
	TOK_TEST = 1,
	TOK_LS,
	TOK_PWD,
	TOK_CD,
	TOK_CAT,
	TOK_DF,
	TOK_CURRENT,
	TOK_TAKE_MESURE,
	TOK_DATE,
	TOK_PING,
	TOK_RECSTART,
	TOK_GET_INTERVAL_VARIANTS,
	TOK_STOP_RECORDING,
	TOK_ATmAB,
};

typedef struct tokenstruct {
	uint8_t token;
	const char const *keyword;
} tokenstruct;

void process_command(char* cmd);
void parse_cmdline(char* cmdbuf, int16_t *tokens);

#endif /* SH_H_ */