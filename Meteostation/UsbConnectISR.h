/*
 * UsbConnectISR.h
 *
 * Created: 15.05.2014 15:39:32
 *  Author: tolyan
 */ 


#ifndef USB_CONNECT_ISR_H_
#define USB_CONNECT_ISR_H_

#include <stdbool.h>

#define USB_CON_SIG_ACTIVE_LVL	0
#define USB_CON_SIG_PIN_N		7
#define USB_CON_SIG_DDR			DDRE
#define USB_CON_SIG_PORT		PORTE
#define USB_CON_SIG_PIN			PINE

enum USB_STATE
{
	_USB_DISABLED = 1 << 0,
	_USB_ENABLED = 1 << 1,
	
	USB_CHANGE_STATUS_REQ = 1 << 2,
	USB_STARTUP			  = 1 << 3
};

void USBConnectTrace_Enable(char enable);
uint8_t isUSBConnected();

extern enum USB_STATE USB_Status;

#endif /* USB_CONNECT_ISR_H_ */