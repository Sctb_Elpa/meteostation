﻿/*
 * GccUSB.cpp
 *
 * Created: 29.04.2014 8:11:08
 *  Author: max
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <stdio.h>

#include "Config/AppConfig.h"

#include "Lib/FatFs/ff.h"

#include "SoftI2CMaster.h"
#include "SSD1306.h"
#include "ds1307.h"
#include "MassStorageGenericHID.h"
#include "sht.h"
#include "Fonts.h"
#include "INT0.h"
#include "UsbConnectISR.h"
#include "Display.h"
#include "Counters.h"
#include "Charge_ctl.h"
#include "Shaduler.h"
#include "Settings.h"
#include "UI.h"
#include "FileIO.h"
#include "Lib/24cXXX.h"
#include "BT.h"
#include "FastShadulerTimer.h"
#include "debug.h"
	
static void tryMount()
{
	FRESULT res = mount_extFlash();
	if ((res == FR_NO_FILESYSTEM) || firstRun())
	{
		res = format_extFlash();
		if (res != FR_OK)
			DEBUG_MSG("FF (%i)", res);
	}
	else
		if (res)
			DEBUG_MSG("MFF (%i)", res);
}

#if PWR_REDUCTION_CTRL == 1
static void disableUnesusaryModulesClock(void)
{
	PRR0 = (1 << PRTWI) | (1 << PRSPI)/* | (1 << PRTIM2)*/;
}
#endif
	
static void init_all()
{
#if USE_WDT == 1
	#if WDT_STACK_USAGE == 1
		__asm__ __volatile__ (  \
		"in __tmp_reg__,__SREG__" "\n\t"    \
		"cli" "\n\t"    \
		"wdr" "\n\t"    \
		"sts %0,%1" "\n\t"  \
		"out __SREG__,__tmp_reg__" "\n\t"   \
		"sts %0,%2" "\n\t" \
		: /* no outputs */  \
		: "M" (_SFR_MEM_ADDR(_WD_CONTROL_REG)), \
		"r" (_BV(_WD_CHANGE_BIT) | _BV(WDE)), \
		"r" ((uint8_t) ((WDTO_1S & 0x08 ? _WD_PS3_MASK : 0x00) | _BV(WDIE) | _BV(WDE) | (WDTO_1S & 0x07)) ) \
															/*   interrupt   go to reset*/ \
		: "r0"  \
		);
	#else
		wdt_enable(WDTO_1S);
	#endif
#endif
	SetupHardware(); // LUFA hardware init operations	
	
#if PWR_REDUCTION_CTRL == 1
	disableUnesusaryModulesClock();
#endif
	
	i2c_init(); // I2C
	i2c_SetSpeed(I2C_SPEED_FAST);
	
	/*
	UHWCON &= ~UIDE;
	UHWCON |= UIMOD;
	*/				
	ds1307_init(); // Часы
	comreset(); //датчик температуры и влажности
	
#if FAST_SHADULER == 1
	InitFastShadulerTimer(1);
#else
	INT0_Enable(1); //Часовое прерывание
#endif
	USBConnectTrace_Enable(1); // включить слежение за USB
	initCharge_Ctrl(); // слежение за зарядкой
			
#if DISPLAY_ENABLED == 1	
	ButtonsInit(); //кнопки
#endif
	
	UpdateCallbacks(UI_DEFAULT); // настройка кнопок на состояние по умолчанию

	set_sleep_mode(SLEEP_MODE_IDLE/*SLEEP_MODE_PWR_DOWN*/); // режим сна
	ACSR &= ~(1 << ACD); // disable analog comparator
	
	tryMount();

	Shaduler_enable(1); //планировщик

	GlobalInterruptEnable(); // sei()
	
#if WATCH_STACK == 1
	struct stackInfo lastDBGInfo;
	ds1307_readNVRAM(&lastDBGInfo, NVRAM_DEBUG_OFFSET, sizeof(struct stackInfo));
	if (lastDBGInfo._SP)
		DEBUG_MSG("LCRP:%i SP:%x", lastDBGInfo.poscode, lastDBGInfo._SP);		
	lastDBGInfo._SP = 0;
	ds1307_writeNVRAM(NVRAM_DEBUG_OFFSET, &lastDBGInfo, sizeof(struct stackInfo));
#endif
	
	BT_Init(); // must be after GlobalInterruptEnable()!
}

void setDateTime()
{
	static struct ds1307DateTime initdate = 
	{
		.second = 0,
		.minute = 0,
		.hour = 0,
		.DoW = 0,
		.day = 1,
		.month = 1,
		.year = 0		
	};
	ds1307_setdate(&initdate);
}
#if DEBUG_BAD_ISR == 1
static int counter = 0;
#endif

int main(void)
{	  
	init_all();

	//setDateTime(); // Установить в часы дату (тест)
	
	while(1)
    {	
		if (tickPassed)
		{
#if USE_WDT == 1
			wdt_reset();
#endif
			tickPassed = 0;
			Tick();
		}
		if (forceDisplayRefrash)
		{
#if DISPLAY_ENABLED == 1			
			RefrashDisplay();
#endif
			forceDisplayRefrash = 0;
		}

		if (USB_Status & USB_CHANGE_STATUS_REQ)
		{
			if (!(USB_Status & USB_STARTUP))
				forceDisplayRefrash = 1;
				
			uint8_t usbStatus = isUSBConnected();
			MesureEnableVoteing(USB_VOTE, usbStatus);
			if (usbStatus)
			{
				// usb connected
				disableSaveMode();
				umount_extFlash();
				
#if USB_ENABLED == 1	
#if PWR_REDUCTION_CTRL == 1
				PRR1 &= ~(1 << PRUSB);		
				_delay_us(10);	
#endif
				USB_Init();
#endif				
				USB_Status = _USB_ENABLED;
				if (forceDisplayRefrash)
					dataOutputVars.popUp = POPUP_USB_CONNECTED;
			}
			else
			{
				// usb disconnected
#if USB_ENABLED == 1				
				USB_Disable();
#if PWR_REDUCTION_CTRL == 1
				PRR1 |= 1 << PRUSB;
#endif
#endif				
				tryMount();
				
				USB_Status = _USB_DISABLED;
				if (forceDisplayRefrash)
					dataOutputVars.popUp = POPUP_USB_DISCONNECTED;
			}	
		}
		if (uart_is_data_avalable())
			ProcessBT();
						
		firstRunFinished();
#if DEBUG_BAD_ISR == 1
		sprintf(dataOutputVars.dbgStr, "bISR=%d", counter);
#endif

#if USE_WDT == 1
		wdt_reset();
#endif	

#if USB_ENABLED == 1		
		if (USB_Status == _USB_ENABLED)
		{
			// process USB events
			LUFA_main();
			continue;
		}
#endif	
		sleep_cpu();
    }
}


// default interrupt routine
ISR(BADISR_vect)
{
#if DEBUG_BAD_ISR == 1	
	++counter;
#endif	
}

#if WDT_STACK_USAGE == 1
ISR(WDT_vect)
{
	// все плохо! вылет по собаке, поэтому сохраним стек
	DEBUG_SAVE_SP(1);	
}
#endif