/*
 * BT.c
 *
 * Created: 28.08.2014 15:42:19
 *  Author: tolyan
 */ 

#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>
#include <avr/wdt.h>

#include "Config/AppConfig.h"

#include "debug.h"

#include "Display.h"
#include "sh.h"
#include "Shaduler.h"
#include "SSD1306.h"
#include "Settings.h"

#include "BT.h"

static const char set3800CMD[] PROGMEM = "AT+AB ChangeDefaultBaud 38400\n\r";

// config
static const char configStrs[] PROGMEM = "AT+AB config DeviceName = SCTB_Meteostation\0\
AT+AB config AllowSniff = true\0\
AT+AB config InquiryScan = true\0\
AT+AB config HostDeepSleepEnable = true\0\
AT+AB config RmtEscapeSequence = true\0\
AT+AB config EnableIAP = false\0\
AT+AB config COD = 080000\0";
//@#@$@%

// goto deep sleep
static const char AllowSniffOff[] PROGMEM = "AT+AB config AllowSniff = false\n\r";
static const char InquiryScanOff[] PROGMEM = "AT+AB config InquiryScan = false\n\r";
static const char gotoDeepSleep1[] PROGMEM = "AT+AB config HostDeepSleepEnable = true\n\r";
static const char gotoDeepSleep2[] PROGMEM = "AT+AB config GPIO_HostKeepAwake = 3\n\r";
static const char gotoDeepSleep3[] PROGMEM = "AT+AB config GPIO_HostWakeup = 3\n\r";
static const char gotoDeepSleep4[] PROGMEM = "AT+AB config PageScan = false\n\r";

// set name
static const char setNameCMD[] PROGMEM = "AT+AB config DeviceName = SCTB_Meteostation\n\r";

//visable
static const char setVisableCMD[] PROGMEM = "AT+AB UpdateInquiryScan %c\n\r";

//remote cmd
static const char remoteCMD[] PROGMEM = "AT+AB config RmtEscapeSequence = true\n\r";

// disable iAP
static const char disableIAP[] PROGMEM = "AT+AB config EnableIAP = false\n\r";

static const char HostToModule[] PROGMEM = "AT+AB";
static const char ModuleToHost[] PROGMEM = "AT-AB";
static const char Config[] PROGMEM = "config";
static const char CommandMode[] PROGMEM = "-CommandMode-";
static const char BDAddress[] PROGMEM = "BDAddress";
static const char EnableBond[] PROGMEM = "EnableBond";
static const char BypassMode[] PROGMEM = "-BypassMode-";
static const char EscapeSequence[] PROGMEM = "^ #$^%\n\r";
static const char SPPDisconnect[] PROGMEM = "SPPDisconnect";
static const char InquiryScanVarName[] PROGMEM = "InquiryScan";
static const char ConnectionDown[] PROGMEM = "ConnectionDown";
static const char ConfigOk[] PROGMEM = "ConfigOk";

const char newLine[] PROGMEM = "\n\r";

static enum enMode mode;
static int16_t BT_disavleVisableCounter = -1;

static FILE uartStream = FDEV_SETUP_STREAM(uart_putc, uart_getc, _FDEV_SETUP_RW);

static uint8_t init_done;

#define UART_PRR 1

static void resetBTModule()
{
	BT_RESET_PORT |= 1 << BT_RESET_PIN_N;
	BT_RESET_DDR |= 1 << BT_RESET_PIN_N;
	BT_RESET_PORT &= ~(1 << BT_RESET_PIN_N);
	_delay_ms(1);
	BT_RESET_PORT |= 1 << BT_RESET_PIN_N;
	BT_RESET_DDR &= ~(1 << BT_RESET_PIN_N);
}

void BT_visableCountdown()
{
	if (BT_disavleVisableCounter >= 0)
	{
		if (BT_disavleVisableCounter == 0)
			BT_SetVisable(0);
		--BT_disavleVisableCounter;				
	}
}

void BT_Init()
{
	if (firstRun())
	{
		// init UART
		uart_init(UART_BAUD_SELECT(115200, F_CPU));
		resetBTModule();
		for (uint16_t i = 0; i  < 3000; i += 10)
		{
			_delay_ms(10);
#if USE_WDT == 1
			wdt_reset();
#endif
		}
		uart_puts_p(set3800CMD);
		_delay_ms(150);
#if USE_WDT == 1
		wdt_reset();
#endif
		// reset BT
		resetBTModule();
		init_done = 0;
	} else {
		resetBTModule();
		init_done = 1;
	}

	mode = MODE_INIT;

	uart_init(UART_BAUD_SELECT(BT_BOUD_RATE, F_CPU));
	
	stdin = &uartStream;
	stdout = &uartStream;
}

void BT_SetVisable(uint8_t visable)
{
	if (mode == MODE_COMMAND)
	{
		MesureEnableVoteing(BT_VOTE, visable);
#if INQUERY_SCAN_UPDATE == 1
		char enable; // '0'/'2'
#endif
		if (visable)
		{
#if UART_PRR && PWR_REDUCTION_CTRL == 1
			PRR1 &= ~(1 << PRUSART1); // uart clock enable
			_delay_us(10);
#endif
			UCSR1B |= 1<<RXEN1; // enable ressiver
			dataOutputVars.popUp = POPUP_BT_ENABLED;
#if INQUERY_SCAN_UPDATE == 1
			enable = '2';
#endif
			BT_disavleVisableCounter = BT_AUTODISABLE_TIMEOUT;
			dataOutputVars.headerStatus |= H_BLUETOOTH;
#if FAST_SHADULER == 0
			set_sleep_mode(SLEEP_MODE_IDLE); // ����� ��� � ��������� ��������� BT
#endif
		}
		else
		{
			UCSR1B &= ~(1<<RXEN1);
#if INQUERY_SCAN_UPDATE == 1
			enable = '0';
#endif
			BT_disavleVisableCounter = -1;
			dataOutputVars.headerStatus &= ~H_BLUETOOTH;
#if FAST_SHADULER == 0
			set_sleep_mode(SLEEP_MODE_PWR_DOWN); //�������� ����� ���
#endif
		}
#if INQUERY_SCAN_UPDATE == 1
		printf_P(setVisableCMD, enable);
#endif
		if (!visable)
		{
			UCSR1B &= ~(1<<RXEN1); // disable ressiver*/
#if UART_PRR && PWR_REDUCTION_CTRL == 1
			PRR1 |= 1 << PRUSART1; // uart clock disable
#endif
			if (isDisplayOn())
			{
				dataOutputVars.popUp = POPUP_BT_DISABLED;
				forceDisplayRefrash = 1;
				return;
			}
			else
				return;
		}
		forceDisplayRefrash = 1;
	}
}
	
void ProcessBT()
{
	uint16_t strLen = uart_get_string_len();
	
	if (strLen != UART_NO_DATA)
	{
		char parceBuffer[strLen + 1];
		parceBuffer[strLen] = '\0';
		
		for (uint16_t i = 0; i < strLen; ++i)
			parceBuffer[i] = (char)uart_getc();
		
		for (;;)
		{
			uint16_t t = uart_peekc();
			if ((t == UART_NO_DATA) || (((char)t != '\r') && ((char)t != '\n')))
				break;
			else
				uart_getc();
		}
		
		//
		//strncpy(dataOutputVars.dbgStr, parceBuffer, 60);
		
		switch (mode)
		{
			case MODE_INIT:
				if ((strstr_P(parceBuffer, CommandMode) != NULL) ||
					(strstr_P(parceBuffer, ConnectionDown) != NULL))
				{
					mode = MODE_COMMAND;
					break;
				}
				if (strstr_P(parceBuffer, BypassMode) != NULL)
				{
					mode = MODE_DATA;
					break;
				}
				break;
				
			case MODE_COMMAND:
				if (strstr_P(parceBuffer, BypassMode) != NULL)
				{
					mode = MODE_DATA;
					BT_disavleVisableCounter = -1; // disable autooff
					break;
				}
				// ���������� ���
				if (strstr_P(parceBuffer, BDAddress) != NULL)
				{
					if (!init_done)
					{
						char* p = (char*)configStrs;
						for (;;)
						{
							uart_puts_p(p);
							uart_puts_p(newLine);
#if USE_WDT == 1
							wdt_reset();
#endif					
							_delay_ms(10);
							p += strlen_P(p) + 1;
						
							if (pgm_read_byte(p) == '\0')
								break;	
						}
						_delay_ms(1000);
						init_done = 1;
					}

					BT_SetVisable(0);		
				}
				
				break;
			case MODE_DATA:
				if (strstr_P(parceBuffer, ConnectionDown) != NULL)
				{
					mode = MODE_COMMAND;
					BT_disavleVisableCounter = BT_AUTODISABLE_TIMEOUT; // reset autooff
					break;
				}
				if (*parceBuffer)
					process_command(parceBuffer);
				
				break;
		}
	}
}