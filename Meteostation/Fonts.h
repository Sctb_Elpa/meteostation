/*
 * Fonts.h
 *
 * Created: 14.05.2014 13:12:16
 *  Author: tolyan
 */ 


#ifndef FONTS_H_
#define FONTS_H_

#include <stdint.h>

#define FULL_TABLE						1
#define CUT_OFF_SMALL_LETERS			0

typedef struct FONT_DEF {
	uint8_t u8Width;     			// Character width for storage
	uint8_t u8Height;  			// Character height for storage
	uint8_t *au8FontTable;	// Font table start address in memory
} FONT_DEF;

enum CustomSymbols
{
	BATAREY_START			= 0x10,
	BATAREY_FILLED_SEGMENT  = 0x11,
	BATARY_EMPTY_SEGMENT	= 0x12,
	BATARY_END				= 0x13,
	BATARY_CHARGE_SEGMENT	= 0x1f,
	BLUETOOTH				= 0x20,
	NO_BLUETOOTH			= BLUETOOTH + 1,
	MEMORY_COUNTER_START	= 0x30,
	MEMORY_SEGMENT_EMPTY	= 0x31,
	MEMORY_SEGMENT_FILLED	= 0x32,
	MEMORY_COUNTER_END		= 0x33,
	MEMORY_COUNTER_DUMY		= 0x34,
	TWIN_DOTS				= ':',
	NO_TWIN_DOTS			= TWIN_DOTS + 1,
	RECORDING_SYMBOL		= 0x40,
	NO_RECORDING_SYMBOL		= RECORDING_SYMBOL + 1,
	SNOWFLAKE_1				= 0x50,
	SNOWFLAKE_2				= SNOWFLAKE_1 + 1,
	SNOWFLAKE_3				= SNOWFLAKE_1 + 2,
	SNOWFLAKE_4				= SNOWFLAKE_1 + 3,
	SNOWFLAKE_5				= SNOWFLAKE_1 + 4,
	SNOWFLAKE_6				= SNOWFLAKE_1 + 5,
	SNOWFLAKE_7				= SNOWFLAKE_1 + 6,
	SNOWFLAKE_8				= SNOWFLAKE_1 + 7,
};

#define CHAR_BUFF_SIZE		(4 * 8)

void LoadLeter(char* CharactecterBufer, uint8_t c);

extern const struct FONT_DEF Font_5x7;
extern const struct FONT_DEF Font_Digits_small;
extern struct FONT_DEF Font_Digits_big;
extern struct FONT_DEF FONT_SPECIAL_SYMBOLS;
	
extern const struct FONT_DEF* default_Font;	

#if FULL_TABLE == 1
extern const unsigned char font_5x7[(32 + 65 + 26 + 5 + 127) * 5];
#else
 #if FULL_CUT_OFF_SMALL_LETERS == 1
extern const unsigned char font_5x7[(65 + 5) * 5];
 #else
extern const unsigned char font_5x7[(65 + 26 + 5) * 5];
 #endif
#endif 

extern const uint8_t Snowflake[32 * 8];
	
#endif /* FONTS_H_ */